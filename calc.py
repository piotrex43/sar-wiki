#Copyright © 2023 Frisk
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import json

"""How is this calculated? According to information I were able to obtain from Clogg: First the game chooses the pool to drop from: 287/300 chance for weapons, 12/300 for Armor, and 1/300 chance for 10x Banan. I obtain weapon weights from raw game file with weapon data."""

def return_prob(divided, dividedby):
    """Return percentages"""
    return float(divided)/float(dividedby)

weapons = None
weapons_spawn_ratios = {}
weapons_names = {"GunPistol": "Pistol", "GunMagnum": "Magnum", "GunDeagle": "Deagle", "GunSilencedPistol": "Silenced Pistol", "GunShotgun": "Shotgun", "GunJag7": "JAG-7", "GunSMG": "SMG", "GunThomas": "Thomas Gun", "GunAK": "AK", "GunM16": "M16", "GunDart": "Dogna's Dart Gun", "GunDartEpic": "Dogna's Dartfly Gun", "GunHuntingRifle": "Hunting Rifle", "GunSniper": "Sniper", "GunBow": "Bow", "GunCrossbow": "Sparrow Launcher", "GunEggLauncher": "Big Clunking Gun"}

with open("weapons.json", "r") as weapons_calc:
    weapons = json.load(weapons_calc)

for weapon in weapons:
    if "spawnWeightCaches" in weapon:
        weapons_spawn_ratios[weapon["inventoryID"]] = weapon["spawnWeightCaches"]
        if weapon["inventoryID"] not in weapons_names:
            print("Weapon does not have a name in the file. Please add new weapon to the directory. ID of the weapon:",weapon["inventoryID"])
            sys.exit(1)

total_number = sum([x for x in weapons_spawn_ratios.values()])
print(weapons_spawn_ratios)

for weapon_id, weapon_rate in weapons_spawn_ratios.items():
    chance = (100 * return_prob(weapon_rate,total_number))*(return_prob(287,300))  # First establish probability for weapon dropping in weapon poll, then make it times the probability of drop in rebel chest pool
    print(weapons_names[weapon_id] + ": " + str(round(chance, 2)) + "%")

print("Armor 3 lvl:",str(round(return_prob(12, 300)*100, 2))+"%")  # Armor 3 has 12/300 probablity of dropping
print("10x Banana:",str(round(return_prob(1, 300)*100, 2))+"%")  # 10x Banan has 1/300 probability of dropping

