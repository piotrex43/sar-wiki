return {
	Character = {
		internal = true
	},
	Umbrella = {
		internal = true,
		link = "Umbrella"
	},
	ExpBoost = {
		internal = true
	},
	Gravestone = {
		internal = true,
		link = "Gravestone"
	},
	Emote = {
		internal = true,
		link = "Emote"
	},
	CharacterSkin = {
		internal = true
	},
	DeathExplosion = {
		internal = true
	},
	DNA = {
		internal = true
	},
	Glasses = {
		internal = true,
		link = "Eye Accessories"
	},
	Beard = {
		internal = true,
		link = "Facial Hair"
	},
	Clothes = {
		internal = true,
		link = "Outfits"
	},
	Hat = {
		internal = true,
		link = "Hats"
	},
	GunSkin = {
		internal = true
	},
	Pistol = {
		list = {
			"Yellow Pistol",
			"Blue Pistol",
			"Green Pistol",
			"Orange Pistol",
			"Pink Pistol",
			"Purple Pistol",
			"Red Pistol",
			"Lime Pistol",
			"Toy Pistol",
			"White Pistol",
			"Default Pistol"
		}
	},
	Magnum = {
		list = {
			"Yellow Magnum",
			"Blue Magnum",
			"Green Magnum",
			"Orange Magnum",
			"Pink Magnum",
			"Purple Magnum",
			"Red Magnum",
			"Dark Magnum",
			"Silver Magnum",
			"White Magnum",
			"Wrapped Magnum",
			"Default Magnum"
		}
	},
	["Silenced Pistol"] = {
		list = {
			"Yellow Silenced Pistol",
			"Blue Silenced Pistol",
			"Green Silenced Pistol",
			"Orange Silenced Pistol",
			"Pink Silenced Pistol",
			"Purple Silenced Pistol",
			"Red Silenced Pistol",
			"Golden Silenced Pistol",
			"DNA Silenced Pistol",
			"Elite Silenced Pistol",
			"White Silenced Pistol",
			"Default Silenced Pistol"
		}
	},
	SMG = {
		list = {
			"Yellow SMG",
			"Blue SMG",
			"Green SMG",
			"Orange SMG",
			"Pink SMG",
			"Purple SMG",
			"Red SMG",
			"Hellfire SMG",
			"White SMG",
			"Default SMG"
		}
	},
	Shotgun = {
		list = {
			"Yellow Shotgun",
			"Blue Shotgun",
			"Green Shotgun",
			"Orange Shotgun",
			"Pink Shotgun",
			"Purple Shotgun",
			"Red Shotgun",
			"Military Shotgun",
			"Flower Shotgun",
			"White Shotgun",
			"Blue Knitted Shotgun",
			"Default Shotgun"
		}
	},
	AK = {
		list = {
			"Yellow AK",
			"Blue AK",
			"Green AK",
			"Orange AK",
			"Pink AK",
			"Purple AK",
			"Red AK",
			"Frozen AK",
			"Rebellion AK",
			"White AK",
			"Skeleton AK",
			"Default AK"
		}
	},
	M16 = {
		list = {
			"Yellow M16",
			"Blue M16",
			"Green M16",
			"Orange M16",
			"Pink M16",
			"Purple M16",
			"Red M16",
			"Golden M16",
			"SAW M16",
			"White M16",
			"Default M16"
		}
	},
	Sniper = {
		list = {
			"Yellow Sniper",
			"Blue Sniper",
			"Green Sniper",
			"Orange Sniper",
			"Pink Sniper",
			"Purple Sniper",
			"Red Sniper",
			"Camo Sniper",
			"Sakura Sniper",
			"White Sniper",
			"Candy Corn Sniper",
			"Default Sniper"
		}
	},
	Minigun = {
		list = {
			"Yellow Minigun",
			"Blue Minigun",
			"Green Minigun",
			"Orange Minigun",
			"Pink Minigun",
			"Purple Minigun",
			"Red Minigun",
			"Pineapple Minigun",
			"White Minigun",
			"Default Minigun"
		}
	},
	MeleeSkin = {
		internal = true,
		link = "Melee weapon"
	},
	Token = {
		internal = true
	},
	Magnet = {
		internal = true
	},
	Challenge = {
		internal = true
	},
	Warning = {
		internal = true
	},
	Deagle = {
		list = {
			"Blue Deagle",
			"Green Deagle",
			"Pink Deagle",
			"Purple Deagle",
			"Red Deagle",
			"Yellow Deagle",
			"Orange Deagle",
			"Eagle Deagle",
			"White Deagle",
			"Default Deagle"
		}
	},
	["Dogna's Dart Gun"] = {
		list = {
			"Blue Poison Dart Gun",
			"Green Poison Dart Gun",
			"Orange Poison Dart Gun",
			"Pink Poison Dart Gun",
			"Purple Poison Dart Gun",
			"Red Poison Dart Gun",
			"Yellow Poison Dart Gun",
			"Skunk Poison Dart Gun",
			"White Poison Dart Gun",
			"Default Poison Dart Gun"
		}
	},
	["JAG-7"] = {
		list = {
			"Blue JAG-7",
			"Green JAG-7",
			"Orange JAG-7",
			"Pink JAG-7",
			"Purple JAG-7",
			"Red JAG-7",
			"Yellow JAG-7",
			"Tiger JAG-7",
			"White JAG-7",
			"Default JAG-7"
		}
	},
	["Thomas Gun"] = {
		list = {
			"Blue Thomas Gun",
			"Green Thomas Gun",
			"Orange Thomas Gun",
			"Pink Thomas Gun",
			"Purple Thomas Gun",
			"Red Thomas Gun",
			"Yellow Thomas Gun",
			"Peppermint Thomas Gun",
			"Steampunk Thomas Gun",
			"White Thomas Gun",
			"Default Thomas Gun"
		}
	},
	["Hunting Rifle"] = {
		list = {
			"Lucky Hunting Rifle",
			"Blue Hunting Rifle",
			"Green Hunting Rifle",
			"Orange Hunting Rifle",
			"Pink Hunting Rifle",
			"Purple Hunting Rifle",
			"Red Hunting Rifle",
			"Yellow Hunting Rifle",
			"Rancher Hunting Rifle",
			"White Hunting Rifle",
			"Default Hunting Rifle"
		}
	},
	["Bow & Sparrow"] = {
		list = {
			"Blue Bow & Sparrow",
			"Green Bow & Sparrow",
			"Orange Bow & Sparrow",
			"Pink Bow & Sparrow",
			"Purple Bow & Sparrow",
			"Red Bow & Sparrow",
			"Yellow Bow & Sparrow",
			"Cardinal Bow & Sparrow",
			"White Bow & Sparrow",
			"Default Bow & Sparrow"
		}
	},
	["Sparrow Launcher"] = {
		list = {
			"Blue Sparrow Launcher",
			"Green Sparrow Launcher",
			"Orange Sparrow Launcher",
			"Pink Sparrow Launcher",
			"Purple Sparrow Launcher",
			"Red Sparrow Launcher",
			"Yellow Sparrow Launcher",
			"White Sparrow Launcher",
			"Default Sparrow Launcher"
		}
	},
	Currency = {
		internal = true
	},
	["Super Animal Pass Season 0"] = {
		list = {
			"Pink Cowboy Hat",
			"Lollipop",
			"Black Triangle Shades",
			"3D Glasses",
			"School Uniform (Boy)",
			"School Uniform (Girl)",
			"Lightbulb",
			"Swordfish",
			"SARturday Night Fever",
			"Maid Outfit"
		}
	},
	["Starter Pack DLC"] = {
		list = {
			"Twilight Newsboy Hat",
			"Twilight Overalls",
			"Super Twilight Fennec Fox"
		}
	},
	Beta = {
		list = {
			"Beta Tee",
			"!Keyme Tee",
			"Beta Blue Beanie"
		}
	},
	["Founder's Edition"] = {
		list = {
			"Golden M16",
			"Heart Glasses",
			"Pixile Purple Tee",
			"Pixile White Tee",
			"Halo",
			"Charcoal Suit & Pixile Tie",
			"White Shutter Shades",
			"Golden Silenced Pistol",
			"Emoji (Death) Explosion",
			"Juggling Roulette"
		}
	},
	["SAR Tonight"] = {
		list = {
			"SAR Tonight Umbrella",
			"SAR Tonight Microphone",
			"Tiger Silhouette Tee",
			"SAR Tonight Tee",
			"Donk Tee",
			"Howl Tee",
			"Golden Video Jacket",
			"SAR Tonight Cap",
			"Fox Silhouette Tee"
		}
	},
	["Christmas 2020"] = {
		list = {
			"Peppermint Candy Cane",
			"Festive Sweater",
			"Santa Outfit",
			"Santa Hat"
		}
	},
	["Year of the Super Pig 2019"] = {
		list = {
			"Lunar Shirt",
			"Lunar Dress",
			"Lunar Black Hat",
			"Lunar Red Hat",
			"Lunar Vest",
			"Bamboo Stick",
			"Tai Chi Sword",
			"Lunar Traditional Outfit"
		}
	},
	["Super Duos Day"] = {
		list = {
			"Duos Day Rose"
		}
	},
	["St. Pawtrick's Day 2019"] = {
		list = {
			"Lucky Beard",
			" Lucky Top Hat",
			" Lucky Suit"
		}
	},
	["Moderation items"] = {
		list = {
			"Ruler"
		}
	},
	["Super Artist"] = {
		list = {
			"Paintbrush"
		}
	},
	["Content creator items"] = {
		list = {
			"Purple Robe",
			"Red Robe",
			"Star Glasses"
		}
	},
	["April Fools' Day 2019"] = {
		list = {
			"Banana"
		}
	},
	["Super Easter Event 2019"] = {
		list = {
			"Yellow Bow",
			"Easter Bow",
			"Bunny Ears"
		}
	},
	["DreamHack Dallas 2019"] = {
		list = {
			"DreamHack Umbrella"
		}
	},
	["Pride 2019"] = {
		list = {
			"Baseball Cap (Rainbow)",
			"Rainbow Umbrella"
		}
	},
	["Canada Day 2019"] = {
		list = {
			"Mountie Outfit",
			"Mountie Hat",
			"Hockey Stick"
		}
	},
	["Independence Day 2019"] = {
		list = {
			"Uncle Sam Outfit",
			"Stars & Stripes Hat",
			"Stars & Stripes Baseball Bat"
		}
	},
	["Super Summer Royale 2019"] = {
		list = {
			"Fish Swim Shorts",
			"Floral Swimsuit",
			"White Apron",
			"Hot Dog Fork",
			"Straw Umbrella",
			"Lifebuoy",
			"Meat Skewer",
			"Fruit Skewer",
			"Yellow Lei Hula Skirt",
			"Popsicle",
			"Duck Floatie",
			"Fruit Hat"
		}
	},
	["Super Squids"] = {
		list = {
			"Squid Hat"
		}
	},
	["Northernlion Live Super Show"] = {
		list = {
			"Jeans Vest",
			"Police Hat",
			"Police Outfit",
			"Velvet Robe",
			"Red Button Up Shirt",
			"Red Striped Shirt",
			"Skull Beanie",
			"Egg Umbrella",
			"Josh Umbrella"
		}
	},
	["Super Halloween 2019"] = {
		list = {
			"Halloween Tee",
			"Black Masquerade Mask",
			"Pumpkin Umbrella",
			"Pumpkin Gravestone",
			"Super Bat Mask",
			"Pumpkin Hat",
			"Caveman Costume",
			"Witch's Broom",
			"Pirate's Cutlass",
			"Machete",
			"Candy Corn Umbrella",
			"Witch Hat",
			"Killer Hocky Mask",
			"Spider Umbrella",
			"Skeleton Costume",
			"Spider Dress",
			"Witch Costume",
			"Banana Costume",
			"Vampire Outfit",
			"Super Bat Costume"
		}
	},
	["Day Of The Dead 2019"] = {
		list = {
			"Mariachi Outfit",
			"Mariachi Hat"
		}
	},
	[" Super 1 Year Anniversary"] = {
		list = {
			"Anniversary Cake Gravestone",
			"Pixile Party Hat"
		}
	},
	["Super CRISPRmas 2019"] = {
		list = {
			"Holly Bow",
			"Red Plaid Scarf",
			"Spearmint Candy Cane",
			"Festive Umbrella",
			"Santa Beard",
			"Slay Bells",
			"Blue Down Jacket",
			"Red Nose",
			"CRISPRmas Tree",
			"Peppermint Thomas Gun",
			"CRISPRmas Sweater",
			"Present Umbrella",
			"Elf Hat",
			"Wreath Necklace",
			"Festive Candy Cane",
			"Snowflake Umbrella",
			"Antlers Headband",
			"Elf Outfit",
			"Festive Lights Outfit",
			"Firework",
			"New Years Party Hat",
			"Blue Pom Beanie",
			"White Down Jacket",
			"New Years Dress"
		}
	},
	["Year of the Super Rat"] = {
		list = {
			"Red Paper Fan",
			"Bamboo Hat",
			"Pink Lunar Shirt",
			"Wooden Chopsticks",
			"Dadao",
			"Mandarin Hat",
			"Guandao",
			"Hanfu",
			"Yellow Tang Suit"
		}
	},
	["St. Pawtrick's Day 2020"] = {
		list = {
			"Lucky Bow",
			"Lucky Dress",
			"Lucky Shirt",
			"Lucky Hunting Rifle"
		}
	},
	["Super Easter 2020"] = {
		list = {
			"Blue Bow",
			"Yellow Bow",
			"Pink Bow",
			"Easter Bow",
			"Bunny Ears",
			"Flower Crown",
			"Flower Glasses",
			"Easter Sun Hat",
			"Easter Suspenders",
			"Red Plaid Dress",
			"Eggshell Hat",
			"Easter Suit",
			"Easter Dress",
			"Chocolate Bar"
		}
	},
	["Cherry Blossom Season"] = {
		list = {
			"Sakura Kimono",
			"Sakura Fan",
			"Sakura Umbrella"
		}
	},
	["Summer Royale 2020"] = {
		list = {
			"Blue Sport Sunglasses",
			"White Bermuda Shorts",
			"Blue Polo Shirt",
			"Yellow Plastic Shovel",
			"Vintage Round Glasses",
			"Vintage Floral Swimsuit",
			"Badminton Racket",
			"Vintage Floral Dress",
			"Orange Pastel Overalls"
		}
	},
	["Super Halloween 2020"] = {
		list = {
			"Cauldron Gravestone",
			"Bolts Hat",
			"Skull Bow",
			"Bat Wing Outfit",
			"Skeleton AK",
			"Candycorn Sniper",
			"Frankenstein Outfit",
			"Pumpkin Costume",
			"Devil Pitchfork"
		}
	}
}