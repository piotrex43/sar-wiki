## SAR Wiki Scripts
Contents of this repo:
+ script.py – main script for automation of wiki LUA tables    
+ auth.json.example – example file for auth.json file that contains some auth data for automating wiki edits/downloading data from PlayFab
+ additions.json – file for additions extending the already existing dataset with some information that is not notmally in the PlayFab dataset, mainly used to add descriptions for event items
+ additions_categories.json – similarly as above but for categories, mainly to add links to categories or categorize items
+ LATO.py – custom made script for interacting with MediaWiki wikis, not pretty but solid
+ matched_files.json – a manually compiled list of items and files on the wiki that don't have a standard name

## How to use
The usage of this script should be fairly straightforward, if you don't have Python 3 installed, do it now.
1. Install required packages (only external package is luadata, which is available on pip under `luadata`).
2. Fill out auth.json basing it on auth.json.example, all of the values are actually optional in certain circumstances but filling that out helps with automating stuff.    
When that's done, use
```python
python script.py
```
to start the script.    
There are following arguments that can be used with the script:    
`--no-fetch` – stops the script from fetching the PlayFab catalogue, in this case a file will have to be provided (currently expects `title-D36D-0.11_2.json`)    
`--lang` – language name for the items, it additionally should translate most of the items to given language (use language from game language file, so one of those: tchinese, schinese, russian, spanish)    
`--file name` – if specified it won't attempt saving the output on the wiki itself, instead it will create files in current directory with given name (and some prefix-syntax indicating language and type of LUA table)    

### Required files
There are a few required files before this script can be used, I couldn't add them to the repo because of copyright reasons, however here are files and how to get them:
+ languages.json – a file containing game translations, should be extracted from game data
+ title-D36D-0.11_2.json – not required but if you don't have playfab auth working this is required