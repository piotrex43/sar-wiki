import json, luadata, re, gettext, requests, sys, argparse, os
from collections import defaultdict
from LATO import frisk_wiki_bot

# Setup argument parser
parser = argparse.ArgumentParser(description='Generate wiki LUA tables for generating cosmetics.')
parser.add_argument('--no-fetch', action='store_false', help="Do not fetch catalogue from PlayFab.")
parser.add_argument('--lang', action='store_const', const="", help="Language name for the items, the name is the same as in language file, by default it's English.", default="")
parser.add_argument('--file', help="Provide a filename for saving the output instead of saving directly on the wiki.")
program_args = parser.parse_args()

# Lets store some data from many files, second is the file connecting some of the item names with wiki file names
# which lets us add file argument to such files. Third one is a language file from the game, allowing us to update names to their newest or simply
# allow for getting translations in i18n.

with open('auth.json', 'r') as jsonfile:  # json with auth stuff
	auth = jsonfile.read()
	auth_data = json.loads(auth)

with open('matched_files.json', 'r') as jsonfile:  # file with matched filenames/items
	filenamesmatches = jsonfile.read()
	newfilenamesmatches = json.loads(filenamesmatches)
	
with open('languages.json', 'r') as jsonfile:  # game translation file
	langfileraw = jsonfile.read()
	langfile = json.loads(langfileraw.encode().decode('utf-8-sig'))

if os.path.isfile('last_update.json') is False:
	with open('last_update.json', 'w') as temp:
		temp.write("{}")  # just basic valid JSON
	
wiki = frisk_wiki_bot()

# Downloading PlayFab catalogue
if program_args.no_fetch:
	session_jar = requests.Session()
	session_jar.headers.update({"User-Agent": "SAR-Wiki-Data-Gatherer/1.0", "Content-Type": "application/json", "X-Authorization": auth_data["playfab-auth-token"]})
	playfab_data = session_jar.post("https://d36d.playfabapi.com/Client/GetCatalogItems", data=json.dumps({"CatalogVersion": None}, indent=4))
	# print(playfab_data.text)
	# sys.exit(0)
	try:
		new_json = playfab_data.json()
	except ValueError:
		print("Could not read JSON from PlayFab")
		sys.exit(0)
else:
	with open('catalogue.json', 'r') as jsonfile:  # playfab catalogue
		jsonraw = jsonfile.read()
		new_json = json.loads(jsonraw)


# If this script is used to generate a table for a language wiki, change this to language name. Must match the name from languages.json file (ex. not Russian but russian.
language_to_process = program_args.lang.lower()
language_codes = {"russian": "ru", "spanish": "es", "tchinese": "zh-hans", "schinese": "zh-hant", "": "", "english": ""}
# Code for usage with translation file
try:
	language_code = language_codes[language_to_process]
except KeyError:
	print("This language is not supported!")
	sys.exit(1)
# Setup translation module
translation = gettext.translation('main', localedir='locale', languages=[language_code]) if language_code else gettext.NullTranslations()
_ = translation.gettext
ngettext = translation.ngettext
# Our main "database" we will be building out of the data, will hold the contents of what is written to our ultimate output
important = defaultdict(dict)
	
if program_args.file:
	file_name = "{name}$type{lang}.lua".format(lang="-{code}".format(language_code) if language_code else "", name=program_args.file)
else:
	file_name = None
# If there are items that are unreleased yet add them in here. Such items will be excluded from the output.
blacklist = ["Horus Deagle", "Disco Outfit", "Disco Dress", "Soda Bottle", "White Dragon Umbrella", "Disco Headband", "Red Content Creator Hoodie", "Moosevelt Top Hat", "Moosevelt Glasses", "Moosevelt Outfit", "Thomas Sledison's Outfit", "Thomas Sledison's Hat", "Thanksgiving Hat", "Super Sloth Beanie", "Super Shiba Beanie", "Red Panda Beanie", "Desert SMG", "Super Rotting Fox", "Tanghulu", "Wooden Oar", "White Sailor Hat", "Fisherman Beanie", "White Sailor Dress", "Fisherman Overalls", "Anchor Gravestone", "PC Gamer AK", "PC Gamer Sniper", "PC Gamer SMG", "PC Gamer Biker Jacket", "PC Gamer Umbrella"]  # items not released yet
# First step in the process – for every item in catalogue
for item in new_json['Catalog']:
	# Ignore Items we don't need (bundles, not items, invalid items, or items in the backlist)
	if item['ItemId'].endswith('Bundle') or ('ItemClass' not in item) or (item["ItemClass"] is None) or ('DisplayName' not in item) or (item["DisplayName"] is None) or (item["DisplayName"] in blacklist):
		continue
	# Assign some more important data like item class and itemid, itemid will later be removed and is used internally to connect items with other pieces of data
	important[item['DisplayName']]["class"] = item['ItemClass']
	important[item['DisplayName']]["ItemId"] = item['ItemId']
	# CustomData is a json snippet in itself in the catalogue, there is such data saved there as rarity or currency cost for event items, we only need rarity which is what we do in here
	if 'CustomData' in item and item['CustomData'] is not None:
		try:
			rarity_raw = json.loads(item['CustomData'])
			rarity = int(rarity_raw.get("Rarity", 0))
		except ValueError:
			print("Could not resolve json for {}".format(item['DisplayName']))
	else:
		print("{} doesn't have rarity".format(item['DisplayName']))
		# Rarity will be common if no rarity exists, although this shouldn't happen
		rarity = 0
	important[item['DisplayName']]["rarity"] = rarity
	# This statement will look for English "translation" of an item. Sometimes Display Names in playfab catalogue aren't correct/updated. When it comes to item
	# names the ultimate name is in the language file. Which is why we compare it to language file and if different we add 'name' field to given item with custom name from language file.
	if "Item." + item['ItemId'] in langfile:
		if item['DisplayName'] != langfile["Item." + item['ItemId']]["english"]:
			important[item['DisplayName']]['name'] = langfile["Item." + item['ItemId']]["english"]
	# Annoyingly. files on the wiki are named in very inconsistent way, this creates problems because we cannot assume default file name to be the same as display name for the item
	# which is what this is trying to prevent. It uses matched_files.json file with a list of already discovered item name / filename pairs and assigns 'file' field on matches
	if item['DisplayName'] in newfilenamesmatches["objects"] and newfilenamesmatches["objects"][item['DisplayName']] is not None:
		important[item['DisplayName']]["file"] = newfilenamesmatches["objects"][item['DisplayName']]
	# inconsistencies go even further and... Yeah, we compare the real names with file catalogue just to be sure
	if 'name' in important[item['DisplayName']]:
		if important[item['DisplayName']]['name'] in newfilenamesmatches["objects"]:
			important[item['DisplayName']]["file"] = newfilenamesmatches['objects'][important[item['DisplayName']]['name']]


# GUN MILESTONE GENERATOR
# Alright, so there are guns, which have milestones. And since there is a lot of them, and a lot can change we generate their descriptions in here
# We hold gun kill requirements in here so we can easily lookup how many kills are needed for given skin per weapon.

colors = {'Orange': 1, 'Green': 2, 'Blue': 3, 'Red': 4, 'Purple': 5, 'Yellow': 6, 'Pink': 7, 'White': 8}  
items = ["AK" , "Bow & Sparrow" , "Sparrow Launcher" , "Silenced Pistol" , "Pistol" , "M16" , "Minigun" , "Magnum" , "Shotgun" , "Sniper" , "Hunting Rifle" , "Deagle" , "Poison Dart Gun" , "Thomas Gun" , "SMG" , "JAG-7", "Dual Pistols"]
kills = {"AK": [20, 30, 50, 50, 75, 75, 75, 125], "Bow & Sparrow": [2, 3, 5, 10, 15, 15, 20, 30], "Sparrow Launcher": [2, 3, 5, 10, 15, 15, 20, 30], "Silenced Pistol": [5, 10, 15, 20, 25, 35, 40, 50], "Pistol": [2, 3, 5, 10, 15, 15, 20, 30], 
		 "M16": [10, 15, 20, 25, 30, 40, 50, 60], "Minigun": [2, 3, 5, 10, 15, 15, 20, 30], "Magnum": [5, 5, 10, 15, 20, 25, 30, 40], "Shotgun": [20, 30, 50, 50, 75, 75, 75, 125], "Sniper": [2, 3, 5, 10, 15, 15, 20, 30], "Hunting Rifle": [2, 3, 5, 10, 15, 15, 20, 30], "Deagle": [5, 10, 15, 20, 25, 35, 40, 50], "Poison Dart Gun": [5, 5, 10, 15, 20, 25, 30, 40], "Thomas Gun": [10, 15, 20, 25, 30, 40, 50, 60], "SMG": [20, 30, 50, 50, 75, 75, 75, 125], "JAG-7": [10, 15, 20, 25, 30, 40, 50, 60], "Dual Pistols": [5, 10, 15, 20, 25, 35, 40, 50]}

# Here we assign not just the description, but also the group! Groups allow us to sort items the way we want when they appear on the page so they appear the same as in-game
for item in items:
	kill_requirement = 0
	for num, color_item in enumerate(colors.items()):  # color_name, rarity
		kill_requirement += kills[item][num]
		color_name, rarity = color_item[0], color_item[1]
		important[color_name + ' ' + item]['group'] = rarity
		important[color_name + ' ' + item]['description'] = ngettext("<br>{kills} {weapon} kill", "<br>{kills} {weapon} kills", kill_requirement).format(weapon=item, kills=kill_requirement)

# World isn't perfect, and so isn't the PlayFab catalogue. To be as flexible as possible, the system allows for custom data to be merged with the final table. This data is stored in
# additions.json file. It's a .json file structured similarly to how .json output of this script would look like, so like important dictionary storage. Data can not only be added in here
# but also merged.
with open('additions.json', 'r') as jsonfile:
	additional_data_raw = jsonfile.read()
	additional_data = json.loads(additional_data_raw)
	for key, values in additional_data.items():
		for value_key, value_value in values.items():
			important[key][value_key] = value_value

# Erm, ok. Here we have categories. We check for specific keywords in Display Name of items and assign the right 'link' property and add guns to respective category...
# This code is to be improved later
categories = defaultdict(dict)
for item_name, item_values in important.items():
	if "class" in item_values and item_values["class"] not in categories:
		categories[item_values["class"]] = {"internal": True}
	if "AK" in item_name:
		if "AK" not in categories:
			categories["AK"]["list"] = list()
		categories["AK"]["list"].append(item_name)
		important[item_name]["link"] = "AK"
	elif "Bow & Sparrow" in item_name:
		if "Bow & Sparrow" not in categories:
			categories["Bow & Sparrow"]["list"] = list()
		categories["Bow & Sparrow"]["list"].append(item_name)
		important[item_name]["link"] = "Bow & Sparrow"
	elif "Sparrow Launcher" in item_name:
		if "Sparrow Launcher" not in categories:
			categories["Sparrow Launcher"]["list"] = list()
		categories["Sparrow Launcher"]["list"].append(item_name)
		important[item_name]["link"] = "Sparrow Launcher"
	elif "Silenced Pistol" in item_name:
		if "Silenced Pistol" not in categories:
			categories["Silenced Pistol"]["list"] = list()
		categories["Silenced Pistol"]["list"].append(item_name)
		important[item_name]["link"] = "Silenced Pistol"
	elif "Dual Pistols" in item_name:
		if "Dual Pistols" not in categories:
			categories["Dual Pistols"]["list"] = list()
		categories["Dual Pistols"]["list"].append(item_name)
		important[item_name]["link"] = "Dual Pistols"
	elif "Pistol" in item_name:
		if "Pistol" not in categories:
			categories["Pistol"]["list"] = list()
		categories["Pistol"]["list"].append(item_name)
		important[item_name]["link"] = "Pistol"
	elif "M16" in item_name:
		if "M16" not in categories:
			categories["M16"]["list"] = list()
		categories["M16"]["list"].append(item_name)
		important[item_name]["link"] = "M16"
	elif "Minigun" in item_name:
		if "Minigun" not in categories:
			categories["Minigun"]["list"] = list()
		categories["Minigun"]["list"].append(item_name)
		important[item_name]["link"] = "Minigun"
	elif "Magnum" in item_name:
		if "Magnum" not in categories:
			categories["Magnum"]["list"] = list()
		categories["Magnum"]["list"].append(item_name)
		important[item_name]["link"] = "Magnum"
	elif "Shotgun" in item_name:
		if "Shotgun" not in categories:
			categories["Shotgun"]["list"] = list()
		categories["Shotgun"]["list"].append(item_name)
		important[item_name]["link"] = "Shotgun"
	elif "Sniper" in item_name:
		if "Sniper" not in categories:
			categories["Sniper"]["list"] = list()
		categories["Sniper"]["list"].append(item_name)
		important[item_name]["link"] = "Sniper"
	elif "Hunting Rifle" in item_name:
		if "Hunting Rifle" not in categories:
			categories["Hunting Rifle"]["list"] = list()
		categories["Hunting Rifle"]["list"].append(item_name)
		important[item_name]["link"] = "Hunting Rifle"
	elif "Deagle" in item_name:
		if "Deagle" not in categories:
			categories["Deagle"]["list"] = list()
		categories["Deagle"]["list"].append(item_name)
		important[item_name]["link"] = "Deagle"
	elif "Poison Dart Gun" in item_name:
		if "Dogna's Dart Gun" not in categories:
			categories["Dogna's Dart Gun"]["list"] = list()
		categories["Dogna's Dart Gun"]["list"].append(item_name)
		important[item_name]["link"] = "Dogna's Dart Gun"
		important[item_name]["name"] = item_name.replace("Poison", "Dogna's")
	elif "Thomas Gun" in item_name:
		if "Thomas Gun" not in categories:
			categories["Thomas Gun"]["list"] = list()
		categories["Thomas Gun"]["list"].append(item_name)
		important[item_name]["link"] = "Thomas Gun"
	elif "SMG" in item_name:
		if "SMG" not in categories:
			categories["SMG"]["list"] = list()
		categories["SMG"]["list"].append(item_name)
		important[item_name]["link"] = "SMG"
	elif "JAG-7" in item_name:
		if "JAG-7" not in categories:
			categories["JAG-7"]["list"] = list()
		categories["JAG-7"]["list"].append(item_name)
		important[item_name]["link"] = "JAG-7"


# World isn... Ah, right, I've done that bit already. We do the same as in additions.json but with category file instead. 
with open('additions_categories.json', 'r') as jsonfile:
	additional_data_raw = jsonfile.read()
	additional_data = json.loads(additional_data_raw)
	for key, values in additional_data.items():
		for value_key, value_value in values.items():
			categories[key][value_key] = value_value

# Before we save the category, lets check all of the item names in the categories for translations, if they exist, use the translated name.
# Category names however are still in English, a space for improvement in the future?
if language_code:
	for keycat, category in categories.items():
		if "list" in category:
			new_list = []
			for item in category["list"]:
				try:
					localized_name = langfile["Item." + important[item].get("ItemId", "")][language_to_process]
					new_list.append(localized_name)
				except KeyError:
					print("Could not find entry in {} for {}.".format(language_to_process, item))
					new_list.append(item)
			categories[keycat]["list"] = new_list
# Lets save the luacats.lua, output to Module:ItemCategories page. One thing is done now yay!

if file_name:
	luadata.write(categories, file_name.replace("$type", "categories"), encoding='utf-8', form=True, prefix='return ')
else:
	categories = "return " + luadata.serialize(categories, form=True).replace('\\t', '\t').replace('\\n', '\n')
	reply = wiki.replace_content("animalroyale", categories, "Module:ItemCategories", "Updating from catalogue")
	if reply != True:
		sys.exit(1)


# Lets compile our data and compare it with what catalogue we had last time we generated the entire list. "Why?" I hear you asking, a few reasons
# first, it lets us to see which items were added in newest catalogue update, which is very useful if we want to exclude unreleased items after rerunning this script
# second - this data saves the time. If due to some error suddenly some items somehow disappear from final list... We will notice that. 
# thirdly - WHO DOESN'T LOVE DATA?! Knowing what was added, what has changed and everything else satisfies my curiosity and that's enough of a reason for this code to exist.
if language_to_process == "":
	with open('last_update.json', 'r') as update_json:
		old_items = json.loads(update_json.read())
		old_stuff = []
		new_stuff = []
		for item in old_items.values():
			old_stuff.append(item["ItemId"])
		for key, val in important.items():
			try:
				old_stuff.remove(val["ItemId"])
				for key_inner, value in val.items(): # Check changes in values
					if key not in ('file'):
						if key not in old_items:
							print("Item {} wasn't present?".format(key))
							break
						if key_inner not in old_items[key]:
							print("Value {} wasn't present in the past for {}".format(key_inner, key))
							continue
						if old_items[key][key_inner] != value:
							print("Value of {} for item {} has changed from {} to {} since last time.".format(key_inner,key ,old_items[key][key_inner], value))
			except ValueError:
				new_stuff.append(key)
			except KeyError:
				print("Stopped at {} with {}".format(key, val))
				raise
		for item in old_stuff:
			print("Since last time „{}” has been removed.".format(item))
		for item in new_stuff:
			print("Since last time item „{}” has been added.".format(important[item]))
	with open('last_update.json', 'w') as update_json:
		update_json.write(json.dumps(important, indent = 4))

# Translate item names in the main dict (important) itself for ease of use on language wikis
if language_to_process:
	replaced_dict = important.copy()
else:
	replaced_dict = None

for item in important.keys():
	localized_name = None
	if language_to_process:
		try:
			# Remove English custom names
			if 'name' in replaced_dict[item]:
				del replaced_dict[item]['name']
			# Different approach for getting names for DNAs
			if "DNA" == important[item].get("class", None): 
				localized_name = langfile["DNA."+important[item]["ItemId"]][language_to_process]
			else:
				localized_name = langfile["Item." + replaced_dict[item].get("ItemId", "")][language_to_process]
			replaced_dict[localized_name] = replaced_dict.pop(item)
		except KeyError:
			print("Could not find entry in {} for {}.".format(language_to_process, item))

important = replaced_dict or important

# Finally, clean up our mess to save on size of final dict by removing ItemId fields which are unnecessary on the wiki. We use display names and then cry when names are changed or inconsistent.
for item in important.keys():
	try:
		del important[localized_name or item]["ItemId"] # clean up Ids we don't need in the future
	except KeyError:
		print("Could not find ItemId for {}.".format(language_to_process, item))

# SAVE THE FINAL FILE, luatableSAR.lua will be the output for Module:ItemList. Wooho
if file_name:
	luadata.write({key: values for key, values in sorted(important.items(), key=lambda item: item[1].get('rarity', 0))}, file_name.replace("$type", "items"), encoding='utf-8', form=True, prefix='return ')
else:
	important = "return " + luadata.serialize(important, form=True).replace('\\t', '\t').replace('\\n', '\n')
	reply = wiki.replace_content("animalroyale", important, "Module:ItemList", "Updating from catalogue")
	if reply != True:
		sys.exit(1)

# *confetti everywhere* we are done, now just copy manually the output of those files to the wiki, I could script that in but maybe later. It's time to hunt some animals...
