return {
	["Super Fox"] = {
		class = "Character",
		rarity = 0,
		file = "Char-fox.png"
	},
	["Super Bear"] = {
		class = "Character",
		rarity = 0,
		file = "Char-bear.png"
	},
	["Super Skullcat"] = {
		class = "Character",
		rarity = 0,
		file = "Char-skullcat.png"
	},
	["Super Tiger"] = {
		class = "Character",
		rarity = 0,
		file = "Char-tiger.png"
	},
	["Super Deer"] = {
		class = "Character",
		rarity = 0,
		file = "Char-deer.png"
	},
	["Super Panda"] = {
		class = "Character",
		rarity = 0,
		file = "Char-panda.png"
	},
	["Super Raccoon"] = {
		class = "Character",
		rarity = 0,
		file = "Char-raccoon.png"
	},
	["Super Sloth"] = {
		class = "Character",
		rarity = 0,
		file = "Char-sloth.png"
	},
	["Red Umbrella"] = {
		class = "Umbrella",
		rarity = 0,
		file = "Umbrella_base_red-resources.assets-261.png"
	},
	["Blue Umbrella"] = {
		class = "Umbrella",
		rarity = 0,
		file = "Umbrella_base_blue-resources.assets-4114.png"
	},
	["Green Umbrella"] = {
		class = "Umbrella",
		rarity = 0,
		file = "Umbrella_base_green-resources.assets-3159.png"
	},
	["Orange Umbrella"] = {
		class = "Umbrella",
		rarity = 0,
		file = "Umbrella_base_orange-resources.assets-2715.png"
	},
	["Pink Umbrella"] = {
		class = "Umbrella",
		rarity = 0,
		file = "Umbrella_base_pink-resources.assets-2788.png"
	},
	["Brown Umbrella"] = {
		class = "Umbrella",
		rarity = 0,
		file = "Umbrella_base_brown-resources.assets-3595.png"
	},
	["Yellow Umbrella"] = {
		class = "Umbrella",
		rarity = 0,
		file = "Umbrella_base_yellow-resources.assets-2643.png"
	},
	["Experience Boost +25%"] = {
		class = "ExpBoost",
		rarity = 0
	},
	["Experience Boost +10%"] = {
		class = "ExpBoost",
		rarity = 0
	},
	["Basic Gravestone"] = {
		class = "Gravestone",
		rarity = 0,
		file = "Gravestone_1-resources.assets-113.png"
	},
	["R.I.P. Gravestone"] = {
		class = "Gravestone",
		rarity = 0,
		file = "Gravestone_2-resources.assets-1045.png"
	},
	["Default Dance"] = {
		class = "Emote",
		rarity = 0,
		file = "Default_dance.png"
	},
	["Simple Wave"] = {
		class = "Emote",
		rarity = 0,
		file = "Wave.png"
	},
	["Super Cat"] = {
		class = "Character",
		rarity = 0,
		file = "Char-cat-calico.png"
	},
	["Super Dog"] = {
		class = "Character",
		rarity = 0,
		file = "Char-dog-hound.png"
	},
	["Super Bunny"] = {
		class = "Character",
		rarity = 0,
		file = "Char-rabbit-brown.png"
	},
	["Super Monkey"] = {
		class = "Character",
		rarity = 0,
		file = "Char-monkey.png"
	},
	["Standard (Yellow) Explosion"] = {
		class = "DeathExplosion",
		rarity = 0,
		file = "Death_standard_icon_yellow-resources.assets-199.png"
	},
	["Bear DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Bear.png"
	},
	["Fox DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Fox.png"
	},
	["Cat DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Cat.png"
	},
	["Deer DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Deer.png"
	},
	["Dog DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Dog.png"
	},
	["Monkey DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Monkey.png"
	},
	["Panda DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Panda.png"
	},
	["Bunny DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Bunny.png"
	},
	["Raccoon DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Raccoon.png"
	},
	["Skullcat DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Skullcat.png"
	},
	["Sloth DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Sloth.png"
	},
	["Tiger DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Tiger.png"
	},
	["Green Tee"] = {
		class = "Clothes",
		rarity = 0,
		file = "Clothes_tshirt_green.png"
	},
	["Standard (Green) Explosion"] = {
		class = "DeathExplosion",
		rarity = 0,
		file = "Death_standard_icon_green-resources.assets-295.png"
	},
	["Standard (Blue) Explosion"] = {
		class = "DeathExplosion",
		rarity = 0,
		file = "Death_standard_icon_blue-resources.assets-824.png"
	},
	["Standard (Red) Explosion"] = {
		class = "DeathExplosion",
		rarity = 0,
		file = "Death_standard_icon_red-resources.assets-4237.png"
	},
	["Standard (Pink) Explosion"] = {
		class = "DeathExplosion",
		rarity = 0,
		file = "Death_standard_icon_pink-resources.assets-1971.png"
	},
	["Standard (Orange) Explosion"] = {
		class = "DeathExplosion",
		rarity = 0,
		file = "Death_standard_icon_orange-resources.assets-2436.png"
	},
	["Green AK"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-ak_green.png",
		group = 2,
		description = "<br>50 AK kills",
		link = "AK"
	},
	["Green M16"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-m16_green.png",
		group = 2,
		description = "<br>25 M16 kills",
		link = "M16"
	},
	["Green Magnum"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-magnum_green.png",
		group = 2,
		description = "<br>10 Magnum kills",
		link = "Magnum"
	},
	["Green Minigun"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-minigun_green.png",
		group = 2,
		description = "<br>5 Minigun kills",
		link = "Minigun"
	},
	["Green Pistol"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-pistol_green.png",
		group = 2,
		description = "<br>5 Pistol kills",
		link = "Pistol"
	},
	["Green SMG"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-smg_green.png",
		group = 2,
		description = "<br>50 SMG kills",
		link = "SMG"
	},
	["Green Shotgun"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-shotgun_green.png",
		group = 2,
		description = "<br>50 Shotgun kills",
		link = "Shotgun"
	},
	["Green Silenced Pistol"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-silenced-pistol_green.png",
		group = 2,
		description = "<br>15 Silenced Pistol kills",
		link = "Silenced Pistol"
	},
	["Green Sniper"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-sniper_green.png",
		group = 2,
		description = "<br>5 Sniper kills",
		link = "Sniper"
	},
	["Orange AK"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-ak_orange.png",
		group = 1,
		description = "<br>20 AK kills",
		link = "AK"
	},
	["Orange M16"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-m16_orange.png",
		group = 1,
		description = "<br>10 M16 kills",
		link = "M16"
	},
	["Orange Magnum"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-magnum_orange.png",
		group = 1,
		description = "<br>5 Magnum kills",
		link = "Magnum"
	},
	["Orange Minigun"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-minigun_orange.png",
		group = 1,
		description = "<br>2 Minigun kills",
		link = "Minigun"
	},
	["Orange Pistol"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-pistol_orange.png",
		group = 1,
		description = "<br>2 Pistol kills",
		link = "Pistol"
	},
	["Orange SMG"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-smg_orange.png",
		group = 1,
		description = "<br>20 SMG kills",
		link = "SMG"
	},
	["Orange Shotgun"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-shotgun_orange.png",
		group = 1,
		description = "<br>20 Shotgun kills",
		link = "Shotgun"
	},
	["Orange Silenced Pistol"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-silenced-pistol_orange.png",
		group = 1,
		description = "<br>5 Silenced Pistol kills",
		link = "Silenced Pistol"
	},
	["Orange Sniper"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-sniper_orange.png",
		group = 1,
		description = "<br>2 Sniper kills",
		link = "Sniper"
	},
	Katana = {
		class = "MeleeSkin",
		rarity = 0,
		file = "Katana.png"
	},
	["Super Red Panda"] = {
		class = "Character",
		rarity = 0,
		file = "Char-redpanda.png"
	},
	["Red Panda DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_RedPanda.png"
	},
	["Wolf DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Wolf.png"
	},
	["Super Wolf"] = {
		class = "Character",
		rarity = 0,
		file = "Char-wolf.png"
	},
	["Capybara DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Capybara.png"
	},
	["Super Capybara"] = {
		class = "Character",
		rarity = 0,
		file = "Char-capybara.png"
	},
	None = {
		class = "Token",
		rarity = 0
	},
	["Donkey DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Donkey.png"
	},
	["Super Donkey"] = {
		class = "Character",
		rarity = 0,
		file = "Char-donkey.png"
	},
	["Blue Tee"] = {
		class = "Clothes",
		rarity = 0,
		file = "Clothes_tshirt_blue.png"
	},
	["Lion DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Lion.png"
	},
	["Super Lion"] = {
		class = "Character",
		rarity = 0,
		file = "Char-lion.png"
	},
	["Grey Beanie"] = {
		class = "Hat",
		rarity = 0,
		file = "Hat_beanie_grey-resources.assets-3241.png"
	},
	["Blue Sunglasses"] = {
		class = "Glasses",
		rarity = 0,
		file = "Glasses_sunglasses_blue-resources.assets-842.png"
	},
	["Hyena DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Hyena.png"
	},
	["Super Hyena"] = {
		class = "Character",
		rarity = 0,
		file = "Char-hyena.png"
	},
	["Red Beanie"] = {
		class = "Hat",
		rarity = 0,
		file = "Hat_beanie_red-resources.assets-802.png"
	},
	["Banana Beanie"] = {
		class = "Hat",
		rarity = 0,
		file = "Hat_beanie_yellow-resources.assets-370.png"
	},
	["Sheep DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Sheep.png"
	},
	["Super Sheep"] = {
		class = "Character",
		rarity = 0,
		file = "Char-sheep.png"
	},
	["Pig DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Pig.png"
	},
	["Boar DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Boar.png"
	},
	["Super Pig"] = {
		class = "Character",
		rarity = 0,
		file = "Char-pig.png"
	},
	["Super Boar"] = {
		class = "Character",
		rarity = 0,
		file = "Char-boar.png"
	},
	["Green Sunglasses"] = {
		class = "Glasses",
		rarity = 0,
		file = "Glasses_sunglasses_green-resources.assets-1375.png"
	},
	["Yellow Sunglasses"] = {
		class = "Glasses",
		rarity = 0,
		file = "Glasses_sunglasses_yellow-resources.assets-599.png"
	},
	["DNA Magnet"] = {
		class = "Magnet",
		rarity = 0,
		file = "Super dna magnet-sharedassets0.assets-51.png"
	},
	["Daily Challenge"] = {
		class = "Challenge",
		rarity = 0
	},
	["Songbird DNA"] = {
		class = "DNA",
		rarity = 0
	},
	["Blue Jay DNA"] = {
		class = "DNA",
		rarity = 0
	},
	["Owl DNA"] = {
		class = "DNA",
		rarity = 0
	},
	["Raven DNA"] = {
		class = "DNA",
		rarity = 0
	},
	["Super Songbird"] = {
		class = "Character",
		rarity = 0,
		file = "char_bird-resources.assets-1814.png"
	},
	["Super Blue Jay"] = {
		class = "Character",
		rarity = 0,
		file = "char_bluejay-resources.assets-281.png"
	},
	["Super Owl"] = {
		class = "Character",
		rarity = 0,
		file = "char_owl-resources.assets-2297.png"
	},
	["Super Raven"] = {
		class = "Character",
		rarity = 0,
		file = "char_raven-resources.assets-936.png"
	},
	Warning = {
		class = "Warning",
		rarity = 0
	},
	["Skunk DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Skunk.png"
	},
	["Super Skunk"] = {
		class = "Character",
		rarity = 0,
		file = "Char-skunk.png"
	},
	["Super White Light Sword"] = {
		class = "MeleeSkin",
		rarity = 0,
		file = "Melee_lightsaber_white-resources.assets-260.png"
	},
	["Red Tee"] = {
		class = "Clothes",
		rarity = 0,
		file = "Clothes_tshirt_red.png"
	},
	["Purple Tee"] = {
		class = "Clothes",
		rarity = 0,
		file = "Clothes_tshirt_purple.png"
	},
	["Super Otter"] = {
		class = "Character",
		rarity = 0,
		file = "Char_otter-resources.assets-2549.png"
	},
	["Otter DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Otter.png"
	},
	["Ferret DNA"] = {
		class = "DNA",
		rarity = 0
	},
	["Super Ferret"] = {
		class = "Character",
		rarity = 0,
		file = "Char_ferret-resources.assets-1959.png"
	},
	["Duck DNA"] = {
		class = "DNA",
		rarity = 0
	},
	["Hawk DNA"] = {
		class = "DNA",
		rarity = 0
	},
	["Super Duck"] = {
		class = "Character",
		rarity = 0,
		file = "Char-ducks.png"
	},
	["Super Hawk"] = {
		class = "Character",
		rarity = 0,
		file = "Char-hawks.png"
	},
	["Hedgehog DNA"] = {
		class = "DNA",
		rarity = 0
	},
	["Super Hedgehog"] = {
		class = "Character",
		rarity = 0,
		file = "Char_hedgehog-resources.assets-2522.png"
	},
	["Rat DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Rat.png"
	},
	["Bat DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Bat.png"
	},
	["Super Rat"] = {
		class = "Character",
		rarity = 0,
		file = "Char_rat-resources.assets-4564.png"
	},
	["Super Bat"] = {
		class = "Character",
		rarity = 0,
		file = "Char_bat-resources.assets-915.png"
	},
	["Teal Tank Top"] = {
		class = "Clothes",
		rarity = 0,
		file = "Clothes_tanktop_blue.png"
	},
	["Green Tank Top"] = {
		class = "Clothes",
		rarity = 0,
		file = "Clothes_tanktop_green.png"
	},
	["Orange Tank Top"] = {
		class = "Clothes",
		rarity = 0,
		file = "Clothes_tanktop_orange.png"
	},
	["Purple Tank Top"] = {
		class = "Clothes",
		rarity = 0,
		file = "Clothes_tanktop_purple.png"
	},
	["Green Deagle"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun_deagle_green.png",
		group = 2,
		description = "<br>15 Deagle kills",
		link = "Deagle"
	},
	["Orange Deagle"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun_deagle_orange.png",
		group = 1,
		description = "<br>5 Deagle kills",
		link = "Deagle"
	},
	["Super Chicken"] = {
		class = "Character",
		rarity = 0,
		file = "Char_chicken-resources.assets-3337.png"
	},
	["Super Cow"] = {
		class = "Character",
		rarity = 0,
		file = "Char_cow-resources.assets-4835.png"
	},
	["Chicken DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Chicken.png"
	},
	["Cow DNA"] = {
		class = "DNA",
		rarity = 0
	},
	["Horse DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Horse.png"
	},
	["Super Horse"] = {
		class = "Character",
		rarity = 0,
		file = "Char_horse-resources.assets-6070.png"
	},
	["Green Poison Dart Gun"] = {
		class = "GunSkin",
		rarity = 0,
		name = "Green Dogna's Dart Gun",
		file = "Gun_dart_green-resources.assets-235.png",
		group = 2,
		description = "<br>10 Poison Dart Gun kills",
		link = "Dogna's Dart Gun"
	},
	["Orange Poison Dart Gun"] = {
		class = "GunSkin",
		rarity = 0,
		name = "Orange Dogna's Dart Gun",
		file = "Gun_dart_orange-resources.assets-2005.png",
		group = 1,
		description = "<br>5 Poison Dart Gun kills",
		link = "Dogna's Dart Gun"
	},
	["Parrot DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Parrot.png"
	},
	["Super Parrot"] = {
		class = "Character",
		rarity = 0,
		file = "Char_parrot-resources.assets-632.png"
	},
	["Busted Gravestone"] = {
		class = "Gravestone",
		rarity = 0,
		file = "Gravestone_broken-resources.assets-1158.png"
	},
	["Green JAG-7"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun_jag7_green.png",
		group = 2,
		description = "<br>25 JAG-7 kills",
		link = "JAG-7"
	},
	["Orange JAG-7"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun_jag7_orange.png",
		group = 1,
		description = "<br>10 JAG-7 kills",
		link = "JAG-7"
	},
	["Leopard DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Leopard.png"
	},
	["Super Leopard"] = {
		class = "Character",
		rarity = 0,
		file = "Char_leopard-resources.assets-1469.png"
	},
	["Penguin DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Pigeon.png"
	},
	["Super Penguin"] = {
		class = "Character",
		rarity = 0,
		file = "Char_penguin-resources.assets-1596.png"
	},
	["Green Thomas Gun"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-thomas_gun_green.png",
		group = 2,
		description = "<br>25 Thomas Gun kills",
		link = "Thomas Gun"
	},
	["Orange Thomas Gun"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-thomas_gun_orange.png",
		group = 1,
		description = "<br>10 Thomas Gun kills",
		link = "Thomas Gun"
	},
	["Hippo DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Hippo.png"
	},
	["Super Hippo"] = {
		class = "Character",
		rarity = 0,
		file = "Char_hippo-resources.assets-1270.png"
	},
	["Daily Challenge v2"] = {
		class = "Challenge",
		rarity = 0
	},
	["Squirrel DNA"] = {
		class = "DNA",
		rarity = 0,
		file = "DNA_Squrrel.png"
	},
	["Super Squirrel"] = {
		class = "Character",
		rarity = 0,
		file = "Char-squirrel.png"
	},
	["Green Hunting Rifle"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-rifle_green.png",
		group = 2,
		description = "<br>5 Hunting Rifle kills",
		link = "Hunting Rifle"
	},
	["Orange Hunting Rifle"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Gun-rifle_orange.png",
		group = 1,
		description = "<br>2 Hunting Rifle kills",
		link = "Hunting Rifle"
	},
	["Green Bow & Sparrow"] = {
		class = "GunSkin",
		rarity = 0,
		group = 2,
		description = "<br>5 Bow & Sparrow kills",
		link = "Bow & Sparrow"
	},
	["Orange Bow & Sparrow"] = {
		class = "GunSkin",
		rarity = 0,
		group = 1,
		description = "<br>2 Bow & Sparrow kills",
		link = "Bow & Sparrow"
	},
	["Green Sparrow Launcher"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Green_Sparrow_Launcher.png",
		group = 2,
		description = "<br>5 Sparrow Launcher kills",
		link = "Sparrow Launcher"
	},
	["Orange Sparrow Launcher"] = {
		class = "GunSkin",
		rarity = 0,
		file = "Orange_Sparrow_Launcher.png",
		group = 1,
		description = "<br>2 Sparrow Launcher kills",
		link = "Sparrow Launcher"
	},
	["Weekly Challenge"] = {
		class = "Challenge",
		rarity = 0
	},
	["Daily Challenge v3"] = {
		class = "Challenge",
		rarity = 0
	},
	["Blue Beanie"] = {
		class = "Hat",
		rarity = 0
	},
	["Pigeon DNA"] = {
		class = "DNA",
		rarity = 0
	},
	["Possum DNA"] = {
		class = "DNA",
		rarity = 0
	},
	["Super Pigeon"] = {
		class = "Character",
		rarity = 0,
		file = "Char_pigeon.png"
	},
	["Super Possum"] = {
		class = "Character",
		rarity = 0,
		file = "char-opossum.png"
	},
	["Goat DNA"] = {
		class = "DNA",
		rarity = 0
	},
	["Super Goat"] = {
		class = "Character",
		rarity = 0,
		file = "char_goat.png"
	},
	["Carl Coins"] = {
		file = "Currency Carl coins.png",
		class = "Currency",
		rarity = 0,
		link = "Currency"
	},
	["Super Serum"] = {
		file = "Super serum.png",
		rarity = 0,
		class = "Currency",
		link = "Super Serum"
	},
	["Default AK"] = {
		link = "AK",
		class = "GunSkin",
		rarity = 0,
		file = "Gun-ak grey.png"
	},
	["Default Bow & Sparrow"] = {
		link = "Bow & Sparrow",
		class = "GunSkin",
		rarity = 0,
		file = "Gun bow default.png"
	},
	["Default Sparrow Launcher"] = {
		link = "Sparrow Launcher",
		class = "GunSkin",
		rarity = 0,
		file = "Gun crossbow default.png"
	},
	["Default Silenced Pistol"] = {
		link = "Silenced Pistol",
		class = "GunSkin",
		rarity = 0,
		file = "Gun-silenced-pistol grey.png"
	},
	["Default Pistol"] = {
		link = "Pistol",
		class = "GunSkin",
		rarity = 0,
		file = "Gun-pistol grey.png"
	},
	["Default M16"] = {
		link = "M16",
		class = "GunSkin",
		rarity = 0,
		file = "Gun-m16 grey.png"
	},
	["Default Minigun"] = {
		link = "Minigun",
		class = "GunSkin",
		rarity = 0,
		file = "Gun-minigun grey.png"
	},
	["Default Magnum"] = {
		link = "Magnum",
		class = "GunSkin",
		rarity = 0,
		file = "Gun-magnum grey.png"
	},
	["Default Shotgun"] = {
		link = "Shotgun",
		class = "GunSkin",
		rarity = 0,
		file = "Gun-shotgun grey.png"
	},
	["Default Sniper"] = {
		link = "Sniper",
		class = "GunSkin",
		rarity = 0,
		file = "Gun-sniper grey.png"
	},
	["Default Hunting Rifle"] = {
		link = "Hunting Rifle",
		class = "GunSkin",
		rarity = 0,
		file = "Gun_rifle_grey-resources.assets-1277.png"
	},
	["Default Deagle"] = {
		link = "Deagle",
		class = "GunSkin",
		rarity = 0,
		file = "Gun_deagle_grey.png"
	},
	["Default Poison Dart Gun"] = {
		link = "Dogna's Dart Gun",
		class = "GunSkin",
		rarity = 0,
		file = "Gun_dart_grey-resources.assets-5316.png",
		name = "Default Dogna's Dart Gun"
	},
	["Default Thomas Gun"] = {
		link = "Thomas Gun",
		class = "GunSkin",
		rarity = 0,
		file = "Gun-thomas gun grey.png"
	},
	["Default SMG"] = {
		link = "SMG",
		class = "GunSkin",
		rarity = 0,
		file = "Gun-smg grey.png"
	},
	["Default JAG-7"] = {
		link = "JAG-7",
		class = "GunSkin",
		rarity = 0,
		file = "Gun jag7.png"
	},
	["Red Polka Dot Umbrella"] = {
		class = "Umbrella",
		rarity = 1,
		file = "Umbrella_polkadot_red-resources.assets-1110.png"
	},
	["Blue Polka Dot Umbrella"] = {
		class = "Umbrella",
		rarity = 1,
		file = "Umbrella_polkadot_blue-resources.assets-2124.png"
	},
	["Brown Polka Dot Umbrella"] = {
		class = "Umbrella",
		rarity = 1,
		file = "Umbrella_polkadot_brown-resources.assets-1020.png"
	},
	["Green Polka Dot Umbrella"] = {
		class = "Umbrella",
		rarity = 1,
		file = "Umbrella_polkadot_green-resources.assets-324.png"
	},
	["Orange Polka Dot Umbrella"] = {
		class = "Umbrella",
		rarity = 1,
		file = "Umbrella_polkadot_orange-resources.assets-134.png"
	},
	["Pink Polka Dot Umbrella"] = {
		class = "Umbrella",
		rarity = 1,
		file = "Umbrella_polkadot_pink-resources.assets-312.png"
	},
	["Yellow Polka Dot Umbrella"] = {
		class = "Umbrella",
		rarity = 1,
		file = "Umbrella_polkadot_yellow-resources.assets-2992.png"
	},
	["Skull & Sticks Gravestone"] = {
		class = "Gravestone",
		rarity = 1,
		file = "Gravestone_3-resources.assets-2759.png"
	},
	Clap = {
		class = "Emote",
		rarity = 1,
		file = "Clap.png"
	},
	["Super Black Bear"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-bear-black.png"
	},
	["Super Tuxedo Cat"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-cat-tuxedo.png"
	},
	["Super Pink Tiger"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-tiger-pink.png"
	},
	["Super Blue Deer"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-deer-blue.png"
	},
	["Super Blue Raccoon"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-raccoon-blue.png"
	},
	["Super Pug"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-dog-pug.png"
	},
	["Super Boston Terrier"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-dog-boston-terrier.png"
	},
	["Super Gray Bunny"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-rabbit-grey.png"
	},
	["Black Secretary Glasses"] = {
		class = "Glasses",
		rarity = 1,
		file = "Glasses_secretary_black-resources.assets-2678.png"
	},
	["Black Round Glasses"] = {
		class = "Glasses",
		rarity = 1,
		file = "Glasses_round_black-resources.assets-1900.png"
	},
	["Moustache 1 (Dark)"] = {
		class = "Beard",
		rarity = 1,
		file = "Moustache1_dark-resources.assets-1044.png"
	},
	["Brown Jacket"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_jacket_brown.png"
	},
	["Blue Striped Tee"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_tshirt_striped_blue.png"
	},
	["Emoji (Angel) Explosion"] = {
		class = "DeathExplosion",
		rarity = 1,
		file = "Death_emoji_angel-resources.assets-1168.png"
	},
	["Emoji (Thumbs Down) Explosion"] = {
		class = "DeathExplosion",
		rarity = 1,
		file = "Death_emoji_icon_thumbsdown-resources.assets-5028.png"
	},
	["Emoji (Broken Heart) Explosion"] = {
		class = "DeathExplosion",
		rarity = 1,
		file = "Death_emoji_broken_heart-resources.assets-372.png"
	},
	["Baseball Cap (Red)"] = {
		class = "Hat",
		rarity = 1,
		file = "Hat_baseball_red-resources.assets-2895.png"
	},
	["Blue AK"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-ak_blue.png",
		group = 3,
		description = "<br>100 AK kills",
		link = "AK"
	},
	["Blue M16"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-m16_blue.png",
		group = 3,
		description = "<br>45 M16 kills",
		link = "M16"
	},
	["Blue Magnum"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-magnum_blue.png",
		group = 3,
		description = "<br>20 Magnum kills",
		link = "Magnum"
	},
	["Blue Minigun"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-minigun_blue.png",
		group = 3,
		description = "<br>10 Minigun kills",
		link = "Minigun"
	},
	["Blue Pistol"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-pistol_blue.png",
		group = 3,
		description = "<br>10 Pistol kills",
		link = "Pistol"
	},
	["Blue SMG"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-smg_blue.png",
		group = 3,
		description = "<br>100 SMG kills",
		link = "SMG"
	},
	["Blue Shotgun"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-shotgun_blue.png",
		group = 3,
		description = "<br>100 Shotgun kills",
		link = "Shotgun"
	},
	["Blue Silenced Pistol"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-silenced-pistol_blue.png",
		group = 3,
		description = "<br>30 Silenced Pistol kills",
		link = "Silenced Pistol"
	},
	["Blue Sniper"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-sniper_blue.png",
		group = 3,
		description = "<br>10 Sniper kills",
		link = "Sniper"
	},
	["Purple AK"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-ak_purple.png",
		group = 5,
		description = "<br>225 AK kills",
		link = "AK"
	},
	["Purple M16"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-m16_purple.png",
		group = 5,
		description = "<br>100 M16 kills",
		link = "M16"
	},
	["Purple Magnum"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-magnum_purple.png",
		group = 5,
		description = "<br>55 Magnum kills",
		link = "Magnum"
	},
	["Purple Minigun"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-minigun_purple.png",
		group = 5,
		description = "<br>35 Minigun kills",
		link = "Minigun"
	},
	["Purple Pistol"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-pistol_purple.png",
		group = 5,
		description = "<br>35 Pistol kills",
		link = "Pistol"
	},
	["Purple SMG"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-smg_purple.png",
		group = 5,
		description = "<br>225 SMG kills",
		link = "SMG"
	},
	["Purple Shotgun"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-shotgun_purple.png",
		group = 5,
		description = "<br>225 Shotgun kills",
		link = "Shotgun"
	},
	["Purple Silenced Pistol"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-silenced-pistol_purple.png",
		group = 5,
		description = "<br>75 Silenced Pistol kills",
		link = "Silenced Pistol"
	},
	["Purple Sniper"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-sniper_purple.png",
		group = 5,
		description = "<br>35 Sniper kills",
		link = "Sniper"
	},
	["Red AK"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-ak_red.png",
		group = 4,
		description = "<br>150 AK kills",
		link = "AK"
	},
	["Red M16"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-m16_red.png",
		group = 4,
		description = "<br>70 M16 kills",
		link = "M16"
	},
	["Red Magnum"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-magnum_red.png",
		group = 4,
		description = "<br>35 Magnum kills",
		link = "Magnum"
	},
	["Red Minigun"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-minigun_red.png",
		group = 4,
		description = "<br>20 Minigun kills",
		link = "Minigun"
	},
	["Red Pistol"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-pistol_red.png",
		group = 4,
		description = "<br>20 Pistol kills",
		link = "Pistol"
	},
	["Red SMG"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-smg_red.png",
		group = 4,
		description = "<br>150 SMG kills",
		link = "SMG"
	},
	["Red Shotgun"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-shotgun_red.png",
		group = 4,
		description = "<br>150 Shotgun kills",
		link = "Shotgun"
	},
	["Red Silenced Pistol"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-silenced-pistol_red.png",
		group = 4,
		description = "<br>50 Silenced Pistol kills",
		link = "Silenced Pistol"
	},
	["Red Sniper"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-sniper_red.png",
		group = 4,
		description = "<br>20 Sniper kills",
		link = "Sniper"
	},
	["Pixile Purple Tee"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_tshirt_pixile_purple.png",
		description = "<br>''[[Founder's Edition]]''"
	},
	["Pixile White Tee"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_tshirt_pixile_white.png",
		description = "<br>''[[Founder's Edition]]''"
	},
	["Teal Striped Sweater"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_sweater_striped.png"
	},
	["Blue Print Tee"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_tshirt_print_blue.png"
	},
	["Green Print Tee"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_tshirt_print_green.png"
	},
	["Super Spider Monkey"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-monkey-black.png"
	},
	["Kinda Funny Tee"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_tshirt_kindafunny.png"
	},
	["Team Fat Tee"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_tshirt_teamfat.png"
	},
	["Super Timber Wolf"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-wolf-brown.png"
	},
	["Black Aviator Sunglasses"] = {
		class = "Glasses",
		rarity = 1,
		name = "Black Aviator Glasses",
		file = "Glasses_rayban_black-resources.assets-3425.png"
	},
	["Blue Aviator Sunglasses"] = {
		class = "Glasses",
		rarity = 1,
		name = "Blue Aviator Glasses",
		file = "Glasses_rayban_blue-resources.assets-947.png"
	},
	Dab = {
		class = "Emote",
		rarity = 1,
		file = "Dab.png"
	},
	["Baseball Cap (Blue)"] = {
		class = "Hat",
		rarity = 1,
		file = "Hat_baseball_blue-resources.assets-1009.png"
	},
	["Baseball Cap (Yellow)"] = {
		class = "Hat",
		rarity = 1,
		file = "Hat_baseball_yellow-resources.assets-1820.png"
	},
	["Super Maroon Fox"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-fox-red.png"
	},
	["Super Pink Fox"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-fox-pink.png"
	},
	["Super Yellow Bear"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-bear-yellow.png"
	},
	["Super Navy Skullcat"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-skullcat-blue.png"
	},
	["Super Sloth Blues"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-sloth-blue.png"
	},
	["Blue Visor"] = {
		class = "Hat",
		rarity = 1,
		file = "Hat_visor_blue-resources.assets-3846.png"
	},
	["Green Visor"] = {
		class = "Hat",
		rarity = 1,
		file = "Hat_visor_green-resources.assets-1303.png"
	},
	["Orange Visor"] = {
		class = "Hat",
		rarity = 1,
		file = "Hat_visor_orange-resources.assets-3687.png"
	},
	["Red Visor"] = {
		class = "Hat",
		rarity = 1,
		file = "Hat_visor_red-resources.assets-374.png"
	},
	["Teal Visor"] = {
		class = "Hat",
		rarity = 1,
		file = "Hat_visor_teal-resources.assets-3696.png"
	},
	["Blue Round Glasses"] = {
		class = "Glasses",
		rarity = 1,
		file = "Glasses_round_blue-resources.assets-4088.png"
	},
	["Maroon Round Glasses"] = {
		class = "Glasses",
		rarity = 1,
		file = "Glasses_round_maroon-resources.assets-2542.png"
	},
	["White Round Glasses"] = {
		class = "Glasses",
		rarity = 1,
		file = "Glasses_round_white-resources.assets-2717.png"
	},
	["Blue Jacket"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_jacket_blue-resources.assets-3074.png"
	},
	["Red Evening Dress"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_night_dress_red-resources.assets-1906.png"
	},
	["Yellow Spring Dress"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_spring_dress_yellow-resources.assets-1025.png"
	},
	["Moustache 1 (Brown)"] = {
		class = "Beard",
		rarity = 1,
		file = "Moustache1_brown-resources.assets-3661.png"
	},
	["Moustache 1 (Light Gray)"] = {
		class = "Beard",
		rarity = 1,
		file = "Moustache1_lightgrey-resources.assets-3958.png"
	},
	["Super Blue Tiger"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-tiger-blue.png"
	},
	["Super Black Cat"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_cat_black-resources.assets-2817.png"
	},
	["Super Mint Deer"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-deer-green.png"
	},
	["Super Yellow Panda"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_panda_yellow-resources.assets-4091.png"
	},
	["Super Red Lion"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-lion-redhead.png"
	},
	["Super Mango Red Panda"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_redpanda_orange-resources.assets-2191.png"
	},
	["Super Pink Capybara"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_capybara_pink-resources.assets-2436.png"
	},
	["Super Swamp Hyena"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-hyena-green.png"
	},
	["Super Green Sheep"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-sheep-green.png"
	},
	["Super Baby Blue Sheep"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-sheep-blue.png"
	},
	Knife = {
		class = "MeleeSkin",
		rarity = 1,
		file = "Melee_knife-resources.assets-4120.png"
	},
	Stick = {
		class = "MeleeSkin",
		rarity = 1,
		file = "Melee-stick.png"
	},
	["Super Woodland Boar"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-boar-fawn.png"
	},
	["Super Maltese Donkey"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-donkey-fawn.png"
	},
	["Super Asinara Donkey"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-donkey-white.png"
	},
	["Super Snowball Pig"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_pig_snowball-resources.assets-4600.png"
	},
	["Moustache 1 (Blonde)"] = {
		class = "Beard",
		rarity = 1,
		file = "Moustache1_blonde-resources.assets-3124.png"
	},
	["Moustache 1 (Gray)"] = {
		class = "Beard",
		rarity = 1,
		file = "Moustache1_grey-resources.assets-3428.png"
	},
	["Moustache 2 (Blonde)"] = {
		class = "Beard",
		rarity = 1,
		file = "Moustache2_blonde-resources.assets-2783.png"
	},
	["Moustache 2 (Brown)"] = {
		class = "Beard",
		rarity = 1,
		file = "Moustache2_brown-resources.assets-4789.png"
	},
	["Moustache 2 (Dark)"] = {
		class = "Beard",
		rarity = 1,
		file = "Moustache2_dark-resources.assets-2654.png"
	},
	["Moustache 2 (Gray)"] = {
		class = "Beard",
		rarity = 1,
		file = "Moustache2_grey-resources.assets-2515.png"
	},
	["Moustache 2 (Light Gray)"] = {
		class = "Beard",
		rarity = 1,
		file = "Moustache2_lightgrey-resources.assets-4137.png"
	},
	["Beard 1 (Blonde)"] = {
		class = "Beard",
		rarity = 1,
		file = "Beard1_blonde-resources.assets-1523.png"
	},
	["Beard 1 (Brown)"] = {
		class = "Beard",
		rarity = 1,
		file = "Beard1_brown-resources.assets-1335.png"
	},
	["Beard 1 (Dark)"] = {
		class = "Beard",
		rarity = 1,
		file = "Beard1_dark-resources.assets-1419.png"
	},
	["Beard 1 (Gray)"] = {
		class = "Beard",
		rarity = 1,
		file = "Beard1_grey-resources.assets-895.png"
	},
	["Beard 1 (Light Gray)"] = {
		class = "Beard",
		rarity = 1,
		file = "Beard1_lightgrey-resources.assets-3176.png"
	},
	["Light Blue Round Glasses"] = {
		class = "Glasses",
		rarity = 1,
		file = "Glasses_round_lightblue-resources.assets-349.png"
	},
	["Brown Aviator Sunglasses"] = {
		class = "Glasses",
		rarity = 1,
		name = "Brown Aviator Glasses",
		file = "Glasses_rayban_brown-resources.assets-2764.png"
	},
	["Green Aviator Sunglasses"] = {
		class = "Glasses",
		rarity = 1,
		name = "Green Aviator Glasses",
		file = "Glasses_rayban_green-resources.assets-4275.png"
	},
	["Blue Secretary Glasses"] = {
		class = "Glasses",
		rarity = 1,
		file = "Glasses_secretary_blue-resources.assets-3282.png"
	},
	["Green Secretary Glasses"] = {
		class = "Glasses",
		rarity = 1,
		file = "Glasses_secretary_green-resources.assets-4697.png"
	},
	["Pink Secretary Glasses"] = {
		class = "Glasses",
		rarity = 1,
		file = "Glasses_secretary_pink-resources.assets-4866.png"
	},
	["Purple Secretary Glasses"] = {
		class = "Glasses",
		rarity = 1,
		file = "Glasses_secretary_purple-resources.assets-763.png"
	},
	["Red Secretary Glasses"] = {
		class = "Glasses",
		rarity = 1,
		file = "Glasses_secretary_red-resources.assets-4543.png"
	},
	["Yellow Secretary Glasses"] = {
		class = "Glasses",
		rarity = 1,
		file = "Glasses_secretary_yellow-resources.assets-3706.png"
	},
	["Orange Sunglasses"] = {
		class = "Glasses",
		rarity = 1,
		file = "Glasses_sunglasses_orange-resources.assets-988.png"
	},
	["Just Sweatpants (Gray)"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_pants_sweatpants-resources.assets-4479.png"
	},
	["Super Canary"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "char_bird_canary-resources.assets-3392.png"
	},
	["Super Blackbird"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "char_bird_blackbird-resources.assets-2766.png"
	},
	["Super Grape Blue Jay"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "char_bluejay_purple-resources.assets-4818.png"
	},
	["Super Barn Owl"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "char_owl_barn-resources.assets-2860.png"
	},
	["Super Pygmy Owl"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "char_owl_pygmy-resources.assets-2687.png"
	},
	["Super Hooded Crow"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "char_raven_hooded-resources.assets-1130.png"
	},
	["ChocoTaco Tee"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_tshirt_chocotaco-resources.assets-1372.png"
	},
	["CrankGameplays Tee"] = {
		class = "Clothes",
		rarity = 1,
		file = "CrankGameplays_Tee.png"
	},
	["Super Grape Skunk"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-skunk-purple.png"
	},
	["Super Green Light Sword"] = {
		class = "MeleeSkin",
		rarity = 1,
		file = "Melee_lightsaber_green-resources.assets-216.png"
	},
	["Red Striped Tee"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_tshirt_striped_red.png"
	},
	["Green Striped Tee"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_tshirt_striped_green.png"
	},
	["Navy Striped Tee"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_tshirt_striped_navy.png"
	},
	["Pink Striped Tee"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_tshirt_striped_pink.png"
	},
	["Red Print Tee"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_tshirt_print_red.png"
	},
	["White Print Tee"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_tshirt_print_white.png"
	},
	["Blue Striped Sweater"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_sweater_striped_blue.png"
	},
	["Pink Striped Sweater"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_sweater_striped_pink.png"
	},
	["Red Striped Sweater"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_sweater_striped_red.png"
	},
	["Super Navy Otter"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_otter_blue-resources.assets-3715.png"
	},
	["Super Chestnut Otter"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_otter_brown-resources.assets-853.png"
	},
	["Super Vanilla Ferret"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_ferret_white-resources.assets-3053.png"
	},
	["Super Green Mallard"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "char_duck_mallard_green-resources.assets-4442.png"
	},
	["Super Blue Mallard"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "char_duck_mallard_blue-resources.assets-3335.png"
	},
	["Super Purple Mallard"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "char_duck_mallard_purple-resources.assets-1597.png"
	},
	["Super Goshawk"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_hawk_goshawk-resources.assets-158.png"
	},
	["Super Osprey"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_hawk_osprey-resources.assets-4482.png"
	},
	["Super Peregrine"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_hawk_peregrine-resources.assets-5014.png"
	},
	["Super Pink Hedgehog"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_hedgehog_pink-resources.assets-3335.png"
	},
	["Super Black Rat"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_rat_black-resources.assets-4003.png"
	},
	["Super Albino Rat"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_rat_white-resources.assets-3670.png"
	},
	["Super Navy Bat"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_bat_blue-resources.assets-932.png"
	},
	["Floral Swim Shorts"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_swimshorts_flower.png"
	},
	["Rebellion Tank Top"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_tanktop_rebellion.png"
	},
	["SAW Tank Top"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_tanktop_SAW.png"
	},
	["White Apron"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_apron.png"
	},
	["Blue Deagle"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun_deagle_blue.png",
		group = 3,
		description = "<br>30 Deagle kills",
		link = "Deagle"
	},
	["Purple Deagle"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun_deagle_purple.png",
		group = 5,
		description = "<br>75 Deagle kills",
		link = "Deagle"
	},
	["Red Deagle"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun_deagle_red.png",
		group = 4,
		description = "<br>50 Deagle kills",
		link = "Deagle"
	},
	["Super Pink Raccoon"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_raccoon_pink-resources.assets-405.png"
	},
	["Super Licorice Red Panda"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char redpanda dark-resources.assets-1707.png"
	},
	["Super Lime Red Panda"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_redpanda_lime-resources.assets-4032.png"
	},
	["Super Blue Wolf"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_wolf_blue-resources.assets-1824.png"
	},
	["Egg Tee"] = {
		class = "Clothes",
		rarity = 1
	},
	["Super Banana Cow"] = {
		class = "Character",
		rarity = 1,
		file = "Char_cow_banana-resources.assets-5367.png"
	},
	["Super Chocolate Cow"] = {
		class = "Character",
		rarity = 1,
		file = "Char_cow_chocolate-resources.assets-2344.png"
	},
	["Super Orange Chicken"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_chicken_red-resources.assets-2349.png"
	},
	["Super Latte Cow"] = {
		class = "Character",
		rarity = 1,
		file = "Char_cow_latte-resources.assets-4882.png"
	},
	["Super Vanilla Cow"] = {
		class = "Character",
		rarity = 1,
		file = "Char_cow_vanilla-resources.assets-3949.png"
	},
	["Super Black Stallion"] = {
		class = "Character",
		rarity = 1,
		file = "Char_horse_black-resources.assets-6216.png"
	},
	["Super White Stallion"] = {
		class = "Character",
		rarity = 1,
		file = "Char_horse_white-resources.assets-4664.png"
	},
	["Blue Poison Dart Gun"] = {
		class = "GunSkin",
		rarity = 1,
		name = "Blue Dogna's Dart Gun",
		file = "Gun_dart_blue-resources.assets-723.png",
		group = 3,
		description = "<br>20 Poison Dart Gun kills",
		link = "Dogna's Dart Gun"
	},
	["Purple Poison Dart Gun"] = {
		class = "GunSkin",
		rarity = 1,
		name = "Purple Dogna's Dart Gun",
		file = "Gun_dart_purple-resources.assets-4966.png",
		group = 5,
		description = "<br>55 Poison Dart Gun kills",
		link = "Dogna's Dart Gun"
	},
	["Red Poison Dart Gun"] = {
		class = "GunSkin",
		rarity = 1,
		name = "Red Dogna's Dart Gun",
		file = "Gun_dart_red-resources.assets-2866.png",
		group = 4,
		description = "<br>35 Poison Dart Gun kills",
		link = "Dogna's Dart Gun"
	},
	Yawn = {
		class = "Emote",
		rarity = 1,
		file = "Yawn.png"
	},
	["Super Blue and Yellow Macaw"] = {
		class = "Character",
		rarity = 1,
		file = "Char_parrot_blue-resources.assets-147.png"
	},
	["Super Green Parrot"] = {
		class = "Character",
		rarity = 1,
		file = "Char_parrot_green-resources.assets-225.png"
	},
	["Mossy Gravestone"] = {
		class = "Gravestone",
		rarity = 1,
		file = "Gravestone_mossy-resources.assets-1079.png"
	},
	["Pointy Fence Gravestone"] = {
		class = "Gravestone",
		rarity = 1,
		file = "Gravestone_iron_fence-resources.assets-1257.png"
	},
	["Blue JAG-7"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun_jag7_blue.png",
		group = 3,
		description = "<br>45 JAG-7 kills",
		link = "JAG-7"
	},
	["Purple JAG-7"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun_jag7_purple.png",
		group = 5,
		description = "<br>100 JAG-7 kills",
		link = "JAG-7"
	},
	["Red JAG-7"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun_jag7_red.png",
		group = 4,
		description = "<br>70 JAG-7 kills",
		link = "JAG-7"
	},
	["Red Tracksuit"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_tracksuit_red-resources.assets-672.png"
	},
	["Blue Tracksuit"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_tracksuit_blue-resources.assets-1130.png"
	},
	["Super Jaguar"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_leopard_jaguar-resources.assets-1447.png"
	},
	["Super Penguin Chick"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_penguin_baby-resources.assets-1253.png"
	},
	["Blue Thomas Gun"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-thomas_gun_blue.png",
		group = 3,
		description = "<br>45 Thomas Gun kills",
		link = "Thomas Gun"
	},
	["Purple Thomas Gun"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-thomas_gun_purple.png",
		group = 5,
		description = "<br>100 Thomas Gun kills",
		link = "Thomas Gun"
	},
	["Red Thomas Gun"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-thomas_gun_red.png",
		group = 4,
		description = "<br>70 Thomas Gun kills",
		link = "Thomas Gun"
	},
	["Red Plaid Scarf"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes_scarf_squares_red-resources.assets-420.png"
	},
	["Holly Bow"] = {
		class = "Hat",
		rarity = 1,
		file = "Hat_holly-resources.assets-322.png"
	},
	["Super Blue Penguin"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_penguin_blue-resources.assets-1132.png"
	},
	["Pink Flower Pin"] = {
		class = "Hat",
		rarity = 1,
		file = "Hat_flower_pink.png"
	},
	["Purple Flower Pin"] = {
		class = "Hat",
		rarity = 1,
		file = "Hat_flower_purple.png"
	},
	["Red Flower Pin"] = {
		class = "Hat",
		rarity = 1,
		file = "Hat_flower_red.png"
	},
	["White Flower Pin"] = {
		class = "Hat",
		rarity = 1,
		file = "Hat_flower_white.png"
	},
	["Yellow Flower Pin"] = {
		class = "Hat",
		rarity = 1,
		file = "Hat_flower_yellow.png"
	},
	["Super Periwinkle Hippo"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_hippo_blue-resources.assets-653.png"
	},
	["Super Orange Hippo"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_hippo_orange-resources.assets-406.png"
	},
	["Green Pool Noodle"] = {
		class = "MeleeSkin",
		rarity = 1,
		file = "Melee_poolnoodle_green.png"
	},
	["Blue Pool Noodle"] = {
		class = "MeleeSkin",
		rarity = 1,
		file = "Melee_poolnoodle_blue.png"
	},
	["Purple Pool Noodle"] = {
		class = "MeleeSkin",
		rarity = 1,
		file = "Melee_poolnoodle_purple.png"
	},
	["Orange Pool Noodle"] = {
		class = "MeleeSkin",
		rarity = 1,
		file = "Melee_poolnoodle_orange.png"
	},
	["Super Gray Squirrel"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-squirrel-grey.png"
	},
	["Blue Hunting Rifle"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-rifle_blue.png",
		group = 3,
		description = "<br>10 Hunting Rifle kills",
		link = "Hunting Rifle"
	},
	["Purple Hunting Rifle"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-rifle_purple.png",
		group = 5,
		description = "<br>35 Hunting Rifle kills",
		link = "Hunting Rifle"
	},
	["Red Hunting Rifle"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-rifle_red.png",
		group = 4,
		description = "<br>20 Hunting Rifle kills",
		link = "Hunting Rifle"
	},
	["Blue Bow & Sparrow"] = {
		class = "GunSkin",
		rarity = 1,
		group = 3,
		description = "<br>10 Bow & Sparrow kills",
		link = "Bow & Sparrow"
	},
	["Purple Bow & Sparrow"] = {
		class = "GunSkin",
		rarity = 1,
		group = 5,
		description = "<br>35 Bow & Sparrow kills",
		link = "Bow & Sparrow"
	},
	["Red Bow & Sparrow"] = {
		class = "GunSkin",
		rarity = 1,
		group = 4,
		description = "<br>20 Bow & Sparrow kills",
		link = "Bow & Sparrow"
	},
	["Blue Sparrow Launcher"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Blue_Sparrow_Launcher.png",
		group = 3,
		description = "<br>10 Sparrow Launcher kills",
		link = "Sparrow Launcher"
	},
	["Purple Sparrow Launcher"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Purple_Sparrow_Launcher.png",
		group = 5,
		description = "<br>35 Sparrow Launcher kills",
		link = "Sparrow Launcher"
	},
	["Red Sparrow Launcher"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Red_Sparrow_Launcher.png",
		group = 4,
		description = "<br>20 Sparrow Launcher kills",
		link = "Sparrow Launcher"
	},
	["Toy Pistol"] = {
		class = "GunSkin",
		rarity = 1,
		description = "<br>''[[S.A.W Shop]] item''",
		file = "Gun_pistol_toy-resources.assets-607.png",
		link = "Pistol"
	},
	["Cardinal Bow & Sparrow"] = {
		class = "GunSkin",
		rarity = 1,
		description = "<br>''[[S.A.W Shop]] item''",
		link = "Bow & Sparrow"
	},
	["Skunk Poison Dart Gun"] = {
		class = "GunSkin",
		rarity = 1,
		name = "Skunk Dogna's Dart Gun",
		file = "Gun dart skunk-resources.assets-1964.png",
		description = "<br>''[[S.A.W Shop]] item''",
		link = "Dogna's Dart Gun"
	},
	["Tiger JAG-7"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun jag7 tiger-resources.assets-247.png",
		description = "<br>''[[S.A.W Shop]] item''",
		link = "JAG-7"
	},
	["Dark Magnum"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun magnum dark-resources.assets-360.png",
		description = "<br>''[[S.A.W Shop]] item''",
		link = "Magnum"
	},
	["Rancher Hunting Rifle"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun-rifle_rancher.png",
		description = "<br>''[[S.A.W Shop]] item''",
		link = "Hunting Rifle"
	},
	["Military Shotgun"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun shotgun military-resources.assets-858.png",
		description = "<br>''[[S.A.W Shop]] item''",
		link = "Shotgun"
	},
	["DNA Silenced Pistol"] = {
		class = "GunSkin",
		rarity = 1,
		file = "Gun silenced pistol DNA-resources.assets-1629.png",
		description = "<br>''[[S.A.W Shop]] item''",
		link = "Silenced Pistol"
	},
	["Pizza Cutter"] = {
		class = "MeleeSkin",
		rarity = 1,
		file = "Melee_pizzacutter-resources.assets-760.png"
	},
	["Banana Spring Dress"] = {
		class = "Clothes",
		rarity = 1
	},
	["Black Headband"] = {
		class = "Hat",
		rarity = 1
	},
	["Red Headband"] = {
		class = "Hat",
		rarity = 1
	},
	["Broken Glasses"] = {
		class = "Glasses",
		rarity = 1,
		file = "Glasses_broken-resources.assets-1641.png"
	},
	["White Secretary Glasses"] = {
		class = "Glasses",
		rarity = 1
	},
	["Warpaint (Black)"] = {
		class = "Glasses",
		rarity = 1,
		file = "Glasses warpaint black-resources.assets-1415.png",
		description = "<br>''[[S.A.W Shop]] item''"
	},
	["Warpaint (White)"] = {
		class = "Glasses",
		rarity = 1,
		file = "Glasses warpaint white-resources.assets-281.png"
	},
	Bokken = {
		class = "MeleeSkin",
		rarity = 1,
		file = "Melee bokken-resources.assets-1220.png"
	},
	["Rolling Pin"] = {
		class = "MeleeSkin",
		rarity = 1,
		file = "Melee rollingpin-resources.assets-1310.png"
	},
	["Rusty Sword"] = {
		class = "MeleeSkin",
		rarity = 1,
		file = "Melee rusty sword-resources.assets-1053.png"
	},
	["Wooden Spoon"] = {
		class = "MeleeSkin",
		rarity = 1,
		file = "Melee_spoon-resources.assets-799.png"
	},
	["Red Umbrella (Melee)"] = {
		class = "MeleeSkin",
		rarity = 1,
		file = "Melee umbrella-resources.assets-1970.png"
	},
	["Green Camo Umbrella"] = {
		class = "Umbrella",
		rarity = 1
	},
	["Gray Camo Umbrella"] = {
		class = "Umbrella",
		rarity = 1
	},
	["Pink Bubble Glasses"] = {
		class = "Glasses",
		rarity = 1,
		file = "Glasses bubble pink-resources.assets-717.png"
	},
	["Purple Bubble Glasses"] = {
		class = "Glasses",
		rarity = 1,
		file = "Glasses bubble purple-resources.assets-495.png"
	},
	["Broken Bottle"] = {
		class = "MeleeSkin",
		rarity = 1,
		file = "Melee broken bottle-resources.assets-1821.png"
	},
	["Recycling Tee"] = {
		class = "Clothes",
		rarity = 1,
		file = "Clothes tshirt recycle-resources.assets-1897.png"
	},
	["Super Brown Pigeon"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char_pigeon_brown.png"
	},
	["Super White Possum"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-opossum-white.png"
	},
	["Super Purple Paw Possum"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-opossum-purple.png"
	},
	["Super Pink Possum"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "Char-opossum-pink.png"
	},
	["Super Bicolor Goat"] = {
		class = "CharacterSkin",
		rarity = 1,
		file = "char_goat_bicolor.png"
	},
	["Aquamarine Parasol"] = {
		class = "Umbrella",
		rarity = 2,
		file = "Umbrella_parisol_aquamarine-resources.assets-1632.png"
	},
	["Blue Parasol"] = {
		class = "Umbrella",
		rarity = 2,
		file = "Umbrella_parisol_blue-resources.assets-786.png"
	},
	["Green Parasol"] = {
		class = "Umbrella",
		rarity = 2,
		file = "Umbrella_parisol_green-resources.assets-1414.png"
	},
	["Pink Parasol"] = {
		class = "Umbrella",
		rarity = 2,
		file = "Umbrella_parisol_pink-resources.assets-2147.png"
	},
	["Violet Parasol"] = {
		class = "Umbrella",
		rarity = 2,
		file = "Umbrella_parisol_violet-resources.assets-984.png"
	},
	["White Parasol"] = {
		class = "Umbrella",
		rarity = 2,
		file = "Umbrella_parisol_white-resources.assets-243.png"
	},
	["Yellow Parasol"] = {
		class = "Umbrella",
		rarity = 2,
		file = "Umbrella_parisol_yellow-resources.assets-2775.png"
	},
	["Skull R.I.P. Gravestone"] = {
		class = "Gravestone",
		rarity = 2,
		file = "Gravestone_4-resources.assets-3546.png"
	},
	["Skull on Cross Gravestone"] = {
		class = "Gravestone",
		rarity = 2,
		file = "Gravestone_5-resources.assets-2089.png"
	},
	["Cheer!"] = {
		class = "Emote",
		rarity = 2,
		file = "Cheer.png"
	},
	["Super Grey Tabby Cat"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-cat-grey-tabby.png"
	},
	["Super Red Tabby Cat"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-cat-red-tabby.png"
	},
	["Super Pink Deer"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-deer-pink.png"
	},
	["Supersonic Fox"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-fox-blue.png"
	},
	["Super Red Bear"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-bear-red.png"
	},
	["Super Shadow Skullcat"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-skullcat-black.png"
	},
	["Super Shiba Inu"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-dog-shiba-inu.png"
	},
	["Super Green Bunny"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-rabbit-green.png"
	},
	Monocle = {
		class = "Glasses",
		rarity = 2,
		file = "Glasses_monacle-resources.assets-3022.png"
	},
	["Beard 3 (Dark)"] = {
		class = "Beard",
		rarity = 2,
		file = "Beard3_dark-resources.assets-1138.png"
	},
	["Beard 5 (Dark)"] = {
		class = "Beard",
		rarity = 2,
		file = "Beard5_dark-resources.assets-3275.png"
	},
	["Moustache 5 (Dark)"] = {
		class = "Beard",
		rarity = 2,
		file = "Moustache5_dark-resources.assets-2370.png"
	},
	["Blue Spring Dress"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_spring_dress_blue.png"
	},
	["Balloons Explosion"] = {
		class = "DeathExplosion",
		rarity = 2,
		file = "Death_icon_balloons-resources.assets-3558.png"
	},
	["70's Explosion"] = {
		class = "DeathExplosion",
		rarity = 2,
		file = "Death_icon_70s-resources.assets-1426.png"
	},
	["Hearts Explosion"] = {
		class = "DeathExplosion",
		rarity = 2,
		file = "Death_icon_hearts-resources.assets-407.png"
	},
	["Emoji (Thinking) Explosion"] = {
		class = "DeathExplosion",
		rarity = 2,
		file = "Death_emoji_thinking-resources.assets-635.png"
	},
	["Emoji (Kisses) Explosion"] = {
		class = "DeathExplosion",
		rarity = 2,
		file = "Death_emoji_icon_kissing-resources.assets-4726.png"
	},
	["Emoji (Sad) Explosion"] = {
		class = "DeathExplosion",
		rarity = 2,
		file = "Death_emoji_sad-resources.assets-1127.png"
	},
	["Emoji (Sleepy) Explosion"] = {
		class = "DeathExplosion",
		rarity = 2,
		file = "Death_emoji_icon_zzz-resources.assets-260.png"
	},
	["Emoji (Death) Explosion"] = {
		class = "DeathExplosion",
		rarity = 2,
		file = "Death_emoji_death-resources.assets-1190.png",
		description = "<br>''[[Founder's Edition]]''"
	},
	["Emoji (Wink) Explosion"] = {
		class = "DeathExplosion",
		rarity = 2,
		file = "Death_emoji_winking-resources.assets-1499.png"
	},
	["Yellow Pistol"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-pistol_yellow.png",
		group = 6,
		description = "<br>50 Pistol kills",
		link = "Pistol"
	},
	["Yellow Magnum"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-magnum_yellow.png",
		group = 6,
		description = "<br>80 Magnum kills",
		link = "Magnum"
	},
	["Yellow Silenced Pistol"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-silenced-pistol_yellow.png",
		group = 6,
		description = "<br>110 Silenced Pistol kills",
		link = "Silenced Pistol"
	},
	["Yellow SMG"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-smg_yellow.png",
		group = 6,
		description = "<br>300 SMG kills",
		link = "SMG"
	},
	["Yellow Shotgun"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-shotgun_yellow.png",
		group = 6,
		description = "<br>300 Shotgun kills",
		link = "Shotgun"
	},
	["Yellow AK"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-ak_yellow.png",
		group = 6,
		description = "<br>300 AK kills",
		link = "AK"
	},
	["Yellow M16"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-m16_yellow.png",
		group = 6,
		description = "<br>140 M16 kills",
		link = "M16"
	},
	["Yellow Sniper"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-sniper_yellow.png",
		group = 6,
		description = "<br>50 Sniper kills",
		link = "Sniper"
	},
	["Yellow Minigun"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-minigun_yellow.png",
		group = 6,
		description = "<br>50 Minigun kills",
		link = "Minigun"
	},
	["Pink AK"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-ak_pink.png",
		group = 7,
		description = "<br>375 AK kills",
		link = "AK"
	},
	["Pink M16"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-m16_pink.png",
		group = 7,
		description = "<br>190 M16 kills",
		link = "M16"
	},
	["Pink Magnum"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-magnum_pink.png",
		group = 7,
		description = "<br>110 Magnum kills",
		link = "Magnum"
	},
	["Pink Minigun"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-minigun_pink.png",
		group = 7,
		description = "<br>70 Minigun kills",
		link = "Minigun"
	},
	["Pink Pistol"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-pistol_pink.png",
		group = 7,
		description = "<br>70 Pistol kills",
		link = "Pistol"
	},
	["Pink SMG"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-smg_pink.png",
		group = 7,
		description = "<br>375 SMG kills",
		link = "SMG"
	},
	["Pink Shotgun"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-shotgun_pink.png",
		group = 7,
		description = "<br>375 Shotgun kills",
		link = "Shotgun"
	},
	["Pink Silenced Pistol"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-silenced-pistol_pink.png",
		group = 7,
		description = "<br>150 Silenced Pistol kills",
		link = "Silenced Pistol"
	},
	["Pink Sniper"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-sniper_pink.png",
		group = 7,
		description = "<br>70 Sniper kills",
		link = "Sniper"
	},
	["Super Pixile Fox"] = {
		class = "CharacterSkin",
		rarity = 2
	},
	["White Shutter Shades"] = {
		class = "Glasses",
		rarity = 2,
		file = "Glasses_shutter_shades_white-resources.assets-2475.png",
		description = "<br>''[[Founder's Edition]]''"
	},
	["Diamonds Sweater"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_sweater_diamonds.png"
	},
	["Red Fancy Shirt"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_shirt_red.png"
	},
	["Purple Fancy Shirt"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_shirt_purple.png"
	},
	["Red College Jacket"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_college_jacket_red.png"
	},
	["Black College Jacket"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_college_jacket_black.png"
	},
	["Super Crimson Monkey"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-monkey-red.png"
	},
	["Maroon Jacket"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_jacket_maroon.png"
	},
	["Super Shady Lion"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-lion-dark.png"
	},
	["Red Sunglasses"] = {
		class = "Glasses",
		rarity = 2,
		file = "Glasses_sunglasses_red-resources.assets-2916.png"
	},
	["Super Golden Lab"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-dog-labrador-golden.png"
	},
	["Super Chocolate Lab"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-dog-labrador-chocolate.png"
	},
	["Super Black Lab"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-dog-labrador-black.png"
	},
	["Captain's Hat"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat_captain-resources.assets-1500.png"
	},
	["Military Hat (Beige)"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat_military_beige-resources.assets-1249.png"
	},
	["Military Hat (Blue)"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat_military_blue-resources.assets-497.png"
	},
	["Military Hat (Green)"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat_military_green-resources.assets-809.png"
	},
	["Blue College Jacket"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_college_jacket_blue.png"
	},
	["White College Jacket"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_college_jacket_white.png"
	},
	["Super Siamese Cat"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-cat-siamese.png"
	},
	["Super Mossy Sloth"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-sloth-green.png"
	},
	["Pink Visor"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat_visor_pink-resources.assets-2640.png"
	},
	["Devil Horns"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat_devil_horns-resources.assets-1113.png"
	},
	["Blue Fancy Shirt"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_shirt_blue.png"
	},
	["Green Fancy Shirt"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_shirt_green.png"
	},
	["Black Jacket"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_jacket_black-resources.assets-2427.png"
	},
	["White Evening Dress"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_night_dress_white-resources.assets-631.png"
	},
	["Pink Spring Dress"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_spring_dress_pink-resources.assets-3742.png"
	},
	["Moustache 5 (Brown)"] = {
		class = "Beard",
		rarity = 2,
		file = "Moustache5_brown-resources.assets-412.png"
	},
	["Moustache 5 (Light Gray)"] = {
		class = "Beard",
		rarity = 2,
		file = "Moustache5_lightgrey-resources.assets-3436.png"
	},
	["Beard 3 (Brown)"] = {
		class = "Beard",
		rarity = 2,
		file = "Beard3_brown-resources.assets-3412.png"
	},
	["Beard 3 (Light Gray)"] = {
		class = "Beard",
		rarity = 2,
		file = "Beard3_lightgrey-resources.assets-3666.png"
	},
	["Beard 5 (Brown)"] = {
		class = "Beard",
		rarity = 2,
		file = "Beard5_brown-resources.assets-1428.png"
	},
	["Beard 5 (Light Gray)"] = {
		class = "Beard",
		rarity = 2,
		file = "Beard5_lightgrey-resources.assets-539.png"
	},
	Laughing = {
		class = "Emote",
		rarity = 2,
		file = "Laugh.png"
	},
	["Super Blue Bear"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-bear-blue.png"
	},
	["Super Brown Raccoon"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-raccoon-brown.png"
	},
	["Super Vanilla Raccoon"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-raccoon-beige.png"
	},
	["Super Red Bunny"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-rabbit-red.png"
	},
	["Super Phantom Sloth"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_sloth_pale-resources.assets-3601.png"
	},
	["Super Blue Panda"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_panda_blue-resources.assets-197.png"
	},
	["Super Purple Panda"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_panda_purple-resources.assets-674.png"
	},
	["Super Shadow Wolf"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-wolf-black.png"
	},
	["Super Berry Red Panda"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_redpanda_blue-resources.assets-2219.png"
	},
	["Super Purple Hyena"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-hyena-purple.png"
	},
	["Super Brown Sheep"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-sheep-brown.png"
	},
	["Super Pink Sheep"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-sheep-pink.png"
	},
	["Golf Club"] = {
		class = "MeleeSkin",
		rarity = 2,
		file = "Melee_golfclub-resources.assets-3163.png"
	},
	["Wood Plank"] = {
		class = "MeleeSkin",
		rarity = 2,
		file = "Melee-woodplank.png"
	},
	Wrench = {
		class = "MeleeSkin",
		rarity = 2,
		file = "Melee-wrench.png"
	},
	Backscratcher = {
		class = "MeleeSkin",
		rarity = 2,
		file = "Melee-backscratcher.png"
	},
	["Super Lunar Pig"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-pig-red.png"
	},
	["Super Dark Boar"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-boar-grey.png"
	},
	["Super Mule"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-donkey-mule.png"
	},
	["Super Saddleback Pig"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_pig_bicolor-resources.assets-4608.png"
	},
	["Moustache 5 (Blonde)"] = {
		class = "Beard",
		rarity = 2,
		file = "Moustache5_blonde-resources.assets-663.png"
	},
	["Moustache 5 (Gray)"] = {
		class = "Beard",
		rarity = 2,
		file = "Moustache5_grey-resources.assets-2621.png"
	},
	["Moustache 4 (Blonde)"] = {
		class = "Beard",
		rarity = 2,
		file = "Moustache4_blonde-resources.assets-1923.png"
	},
	["Moustache 4 (Brown)"] = {
		class = "Beard",
		rarity = 2,
		file = "Moustache4_brown-resources.assets-260.png"
	},
	["Moustache 4 (Dark)"] = {
		class = "Beard",
		rarity = 2,
		file = "Moustache4_dark-resources.assets-2825.png"
	},
	["Moustache 4 (Gray)"] = {
		class = "Beard",
		rarity = 2,
		file = "Moustache4_grey-resources.assets-2042.png"
	},
	["Moustache 4 (Light Gray)"] = {
		class = "Beard",
		rarity = 2,
		file = "Moustache4_lightgrey-resources.assets-3308.png"
	},
	["Beard 3 (Blonde)"] = {
		class = "Beard",
		rarity = 2,
		file = "Beard3_blonde-resources.assets-1715.png"
	},
	["Beard 3 (Gray)"] = {
		class = "Beard",
		rarity = 2,
		file = "Beard3_grey-resources.assets-3882.png"
	},
	["Beard 5 (Blonde)"] = {
		class = "Beard",
		rarity = 2,
		file = "Beard5_blonde-resources.assets-4468.png"
	},
	["Beard 5 (Gray)"] = {
		class = "Beard",
		rarity = 2,
		file = "Beard5_grey-resources.assets-3550.png"
	},
	["Super Wicked Monkey"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_monkey_green-resources.assets-1291.png"
	},
	["Green Robe"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_robe_green-resources.assets-3891.png"
	},
	["Orange Robe"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_robe_orange-resources.assets-4891.png"
	},
	["White Robe"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_robe_white-resources.assets-759.png"
	},
	["Yellow Robe"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_robe_yellow-resources.assets-3763.png"
	},
	["Just Khaki Pants"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_pants_khaki-resources.assets-4716.png"
	},
	["Just Jeans"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes pants jeans-resources.assets-599.png"
	},
	["Geologist's Hammer"] = {
		class = "MeleeSkin",
		rarity = 2,
		file = "Melee_geologist-resources.assets-4413.png"
	},
	["Super Lark"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "char_bird_lark-resources.assets-2198.png"
	},
	["Super Robin"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "char_bird_robin-resources.assets-2981.png"
	},
	["Super Zebra Finch"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "char_bird_zebrafinch-resources.assets-3496.png"
	},
	["Super Bullfinch"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "char_bird_bullfinch-resources.assets-1076.png"
	},
	["Super Berry Blue Jay"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "char_bluejay_lightblue-resources.assets-2396.png"
	},
	["Super Gray Owl"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "char_owl_grey-resources.assets-2190.png"
	},
	["Super Snowy Owl"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "char_owl_snowy-resources.assets-757.png"
	},
	["Super Shadow Raven"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "char_raven_night-resources.assets-4117.png"
	},
	["ChocoTaco Hat"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat_chocotaco-resources.assets-966.png"
	},
	["Super Skunk Blues"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-skunk-petrol.png"
	},
	["Super Blue Light Sword"] = {
		class = "MeleeSkin",
		rarity = 2,
		file = "Melee_lightsaber_blue-resources.assets-3974.png"
	},
	["Super Red Otter"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_otter_red-resources.assets-3505.png"
	},
	["Super Golden Otter"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_otter_yellow-resources.assets-4574.png"
	},
	["Super Cinnamon Ferret"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_ferret_cinnamon-resources.assets-4535.png"
	},
	["Super White Duck"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "char_duck_white-resources.assets-1598.png"
	},
	["Super Black Duck"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "char_duck_black-resources.assets-4369.png"
	},
	["Super Kestrel"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_hawk_kestrel-resources.assets-1630.png"
	},
	["Super Red Hedgehog"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_hedgehog_red-resources.assets-4504.png"
	},
	Carrot = {
		class = "MeleeSkin",
		rarity = 2,
		file = "Melee_carrot-resources.assets-700.png"
	},
	Corn = {
		class = "MeleeSkin",
		rarity = 2,
		file = "Melee_corn.png"
	},
	["Super Spotted Rat"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_rat_spotted-resources.assets-3699.png"
	},
	["Super Shadow Bat"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_bat_grey-resources.assets-1873.png"
	},
	["Floral Swimsuit"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_bikini_flower.png"
	},
	["Polka Dot Swimsuit"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_bikini_polkadots_pink.png"
	},
	["Striped Swimsuit"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_bikini_stripes.png"
	},
	["Palm Swim Shorts"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_swimshorts_palm.png"
	},
	["Hot Dog Fork"] = {
		class = "MeleeSkin",
		rarity = 2,
		file = "Melee_bbq_fork.png"
	},
	["Meat Skewer"] = {
		class = "MeleeSkin",
		rarity = 2,
		file = "Melee_skewer.png"
	},
	["Shia's Headband"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat_headband_shia-resources.assets-1425.png"
	},
	["Pink Deagle"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun_deagle_pink.png",
		group = 7,
		description = "<br>150 Deagle kills",
		link = "Deagle"
	},
	["Yellow Deagle"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun_deagle_yellow.png",
		group = 6,
		description = "<br>110 Deagle kills",
		link = "Deagle"
	},
	["Super Emerald Fox"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_fox_green-resources.assets-631.png"
	},
	["Super Banana Deer"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_deer_yellow-resources.assets-868.png"
	},
	["Super Grape Red Panda"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_redpanda_pastel-resources.assets-1581.png"
	},
	["Super Redder Panda"] = {
		class = "CharacterSkin",
		rarity = 2
	},
	["Super Desert Wolf"] = {
		class = "CharacterSkin",
		rarity = 2
	},
	["Super Striped Hyena"] = {
		class = "CharacterSkin",
		rarity = 2
	},
	["Super Swamp Pig"] = {
		class = "CharacterSkin",
		rarity = 2,
		name = "Super Swamp Pig (UNUSED)"
	},
	["Super Salami Pig"] = {
		class = "CharacterSkin",
		rarity = 2,
		name = "Super Salami Pig (UNUSED)"
	},
	["Super Enchanted Boar"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_boar_teal-resources.assets-201.png"
	},
	["Super Woodland Raccoon"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_raccoon_woodland-resources.assets-1089.png"
	},
	["Super Golden Tabby Tiger"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_tiger_golden_tabby-resources.assets-5082.png"
	},
	["Super Weimaraner"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_dog_labrador_weimaraner-resources.assets-5432.png"
	},
	["Super Matcha Cow"] = {
		class = "Character",
		rarity = 2,
		file = "Char_cow_matcha-resources.assets-3945.png"
	},
	["Super Neapolitan Cow"] = {
		class = "Character",
		rarity = 2,
		file = "Char_cow_neapolitan-resources.assets-5583.png"
	},
	["Super Strawberry Cow"] = {
		class = "Character",
		rarity = 2,
		file = "Char_cow_strawberry-resources.assets-1411.png"
	},
	["Super Black Chicken"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_chicken_black-resources.assets-4433.png"
	},
	["Super Spicy Chicken"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_chicken_spicy-resources.assets-5404.png"
	},
	["Super Stinky Tofu Cow"] = {
		class = "Character",
		rarity = 2,
		file = "Char_cow_stinky_tofu-resources.assets-2034.png"
	},
	["Pink Poison Dart Gun"] = {
		class = "GunSkin",
		rarity = 2,
		name = "Pink Dogna's Dart Gun",
		file = "Gun_dart_pink-resources.assets-1022.png",
		group = 7,
		description = "<br>110 Poison Dart Gun kills",
		link = "Dogna's Dart Gun"
	},
	["Yellow Poison Dart Gun"] = {
		class = "GunSkin",
		rarity = 2,
		name = "Yellow Dogna's Dart Gun",
		file = "Gun_dart_yellow-resources.assets-2561.png",
		group = 6,
		description = "<br>80 Poison Dart Gun kills",
		link = "Dogna's Dart Gun"
	},
	["PBJT!"] = {
		class = "Emote",
		rarity = 2,
		file = "Pbj_time.png"
	},
	["Super Cockatiel"] = {
		class = "Character",
		rarity = 2,
		file = "Char_parrot_cockatiel-resources.assets-1475.png"
	},
	["Super Hyacinth Macaw"] = {
		class = "Character",
		rarity = 2,
		file = "Char_parrot_hyacinth-resources.assets-506.png"
	},
	["Pirate's Cutlass"] = {
		class = "MeleeSkin",
		rarity = 2,
		file = "Melee_cutlass-resources.assets-1519.png"
	},
	["SAW Helmet Gravestone"] = {
		class = "Gravestone",
		rarity = 2,
		file = "Gravestone_SAW-resources.assets-1757.png"
	},
	["Rebellion Helmet Gravestone"] = {
		class = "Gravestone",
		rarity = 2,
		file = "Gravestone_rebellion-resources.assets-1526.png"
	},
	["Sad Flower Gravestone"] = {
		class = "Gravestone",
		rarity = 2,
		file = "Gravestone_flower-resources.assets-446.png"
	},
	["Pink JAG-7"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun_jag7_pink.png",
		group = 7,
		description = "<br>190 JAG-7 kills",
		link = "JAG-7"
	},
	["Yellow JAG-7"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun_jag7_yellow.png",
		group = 6,
		description = "<br>140 JAG-7 kills",
		link = "JAG-7"
	},
	["Black Tracksuit"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_tracksuit_black-resources.assets-351.png"
	},
	["Super Sour Leopard"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_leopard_sour-resources.assets-1544.png"
	},
	["Super Frozen Leopard"] = {
		class = "CharacterSkin",
		rarity = 2
	},
	Fez = {
		class = "Hat",
		rarity = 2,
		file = "Hat_fez.png"
	},
	["Super Adelie Penguin"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_penguin_adelie-resources.assets-1588.png"
	},
	["Pink Thomas Gun"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-thomas_gun_pink.png",
		group = 7,
		description = "<br>190 Thomas Gun kills",
		link = "Thomas Gun"
	},
	["Yellow Thomas Gun"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-thomas_gun_yellow.png",
		group = 6,
		description = "<br>140 Thomas Gun kills",
		link = "Thomas Gun"
	},
	Firework = {
		class = "MeleeSkin",
		rarity = 2,
		file = "Melee_firework.png"
	},
	["Blue Striped Scarf"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_scarf_striped_blue-resources.assets-1164.png"
	},
	["Blue Festive Sweater"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_sweater_xmas2019-resources.assets-1230.png"
	},
	["Blue Pom Beanie"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat_pombeanie_blue.png"
	},
	["Orange Pom Beanie"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat_pombeanie_orange-resources.assets-416.png"
	},
	["Super Poison Penguin"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_penguin_gray-resources.assets-1241.png"
	},
	["Super Bubblegum Penguin"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_penguin_pink-resources.assets-988.png"
	},
	["Super Radioactive Rat"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_rat_radioactive-resources.assets-1129.png"
	},
	["Super Lime Hippo"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_hippo_green-resources.assets-1062.png"
	},
	["Super Rose Hippo"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_hippo_pink-resources.assets-1413.png"
	},
	["Lily Flower"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat_flower_lily.png"
	},
	["Pink Cowboy Hat"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat_cowboy_pink-resources.assets-1501.png",
		description = "<br>''[[Super Edition]] reward''"
	},
	Lollipop = {
		class = "MeleeSkin",
		rarity = 2,
		file = "Melee_lollypop-resources.assets-1167.png",
		description = "<br>''[[Super Edition]] reward''"
	},
	["Rainbow Headband"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat_headband_rainbow.png"
	},
	["Super Black Squirrel"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-squirrel-black.png"
	},
	["Super Vanilla Squirrel"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-squirrel-cream.png"
	},
	["Pink Hunting Rifle"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-rifle_pink.png",
		group = 7,
		description = "<br>70 Hunting Rifle kills",
		link = "Hunting Rifle"
	},
	["Yellow Hunting Rifle"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-rifle_yellow.png",
		group = 6,
		description = "<br>50 Hunting Rifle kills",
		link = "Hunting Rifle"
	},
	["Pink Bow & Sparrow"] = {
		class = "GunSkin",
		rarity = 2,
		group = 7,
		description = "<br>70 Bow & Sparrow kills",
		link = "Bow & Sparrow"
	},
	["Yellow Bow & Sparrow"] = {
		class = "GunSkin",
		rarity = 2,
		group = 6,
		description = "<br>50 Bow & Sparrow kills",
		link = "Bow & Sparrow"
	},
	["Pink Sparrow Launcher"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Pink_Sparrow_Launcher.png",
		group = 7,
		description = "<br>70 Sparrow Launcher kills",
		link = "Sparrow Launcher"
	},
	["Yellow Sparrow Launcher"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Yellow_Sparrow_Launcher.png",
		group = 6,
		description = "<br>50 Sparrow Launcher kills",
		link = "Sparrow Launcher"
	},
	["Lime Pistol"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-pistol_lime.png",
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''",
		link = "Pistol"
	},
	["Frozen AK"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-ak_ice.png",
		description = "<br>''[[The Great Gun Buyback|The Great Gun Buyback]] reward''",
		link = "AK"
	},
	["Flower Shotgun"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun-shotgun_vacation.png",
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''",
		link = "Shotgun"
	},
	["Steampunk Thomas Gun"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun tommy gun steampunk.png",
		description = "<br>''[[S.A.W Shop]] item''",
		link = "Thomas Gun"
	},
	["Silver Magnum"] = {
		class = "GunSkin",
		rarity = 2,
		file = "Gun magnum silver-resources.assets-970.png",
		description = "<br>''[[S.A.W Shop]] item''",
		link = "Magnum"
	},
	["Bo Staff"] = {
		class = "MeleeSkin",
		rarity = 2,
		file = "Melee_bo_staff-resources.assets-742.png"
	},
	["Pullover (Orange)"] = {
		class = "Clothes",
		rarity = 2
	},
	["Pullover (Purple)"] = {
		class = "Clothes",
		rarity = 2
	},
	["Banana Headband"] = {
		class = "Hat",
		rarity = 2
	},
	["White Paper Hat"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat paperhat white-resources.assets-1663.png"
	},
	["Broken Top Hat"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat tophat broken-resources.assets-1531.png"
	},
	["Farmer Outfit"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes farmer-resources.assets-1564.png",
		description = "<br>''[[S.A.W Shop]] item''"
	},
	["Boxing Shorts"] = {
		class = "Clothes",
		rarity = 2
	},
	["Pullover (Fox)"] = {
		class = "Clothes",
		rarity = 2
	},
	["Pullover (Gray)"] = {
		class = "Clothes",
		rarity = 2
	},
	["Suspenders (Smart)"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes suspenders smart-resources.assets-738.png"
	},
	["Gray Vest Outfit"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_vest_grey-resources.assets-2042.png"
	},
	["Blue Shutter Shades"] = {
		class = "Glasses",
		rarity = 2
	},
	["Green Shutter Shades"] = {
		class = "Glasses",
		rarity = 2
	},
	["Pink Shutter Shades"] = {
		class = "Glasses",
		rarity = 2
	},
	["Purple Shutter Shades"] = {
		class = "Glasses",
		rarity = 2,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	["Yellow Shutter Shades"] = {
		class = "Glasses",
		rarity = 2
	},
	["Paisley Headband (Black)"] = {
		class = "Hat",
		rarity = 2
	},
	["Paisley Headband (Blue)"] = {
		class = "Hat",
		rarity = 2
	},
	["Paisley Headband (Green)"] = {
		class = "Hat",
		rarity = 2
	},
	["Paisley Headband (Red)"] = {
		class = "Hat",
		rarity = 2,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	["Baseball Bat"] = {
		class = "MeleeSkin",
		rarity = 2
	},
	Billhook = {
		class = "MeleeSkin",
		rarity = 2,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	Scimitar = {
		class = "MeleeSkin",
		rarity = 2
	},
	["Spiked Mace"] = {
		class = "MeleeSkin",
		rarity = 2,
		file = "Melee spiked mace-resources.assets-1011.png"
	},
	["Squeaky Mallet"] = {
		class = "MeleeSkin",
		rarity = 2,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	["Newspaper Umbrella"] = {
		class = "Umbrella",
		rarity = 2,
		file = "Umbrella_newspaper-resources.assets-455.png"
	},
	["Summer Dress"] = {
		class = "Clothes",
		rarity = 2
	},
	["Punk Jacket"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_jacket_punk-resources.assets-1189.png"
	},
	["Punk Shorts"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_pants_punk-resources.assets-2032.png"
	},
	["Sport Swimsuit"] = {
		class = "Clothes",
		rarity = 2,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	["Wooden R.I.P. Gravestone"] = {
		class = "Gravestone",
		rarity = 2,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	Harpoon = {
		class = "MeleeSkin",
		rarity = 2,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	["Purple Cheerleader Outfit"] = {
		class = "Clothes",
		rarity = 2,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	["Red Kilt"] = {
		class = "Clothes",
		rarity = 2,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	Fork = {
		class = "MeleeSkin",
		rarity = 2,
		file = "Melee fork-resources.assets-1800.png",
		description = "<br>''[[S.A.W Shop]] item''"
	},
	["Heart Boxers"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes boxers heart-resources.assets-1270.png"
	},
	["Cowboy Hat"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat_cowboy-resources.assets-952.png"
	},
	["Twilight Overalls"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_overall_stars-resources.assets-731.png",
		description = "<br>[[Starter Pack|Starter Pack DLC]]"
	},
	["Twilight Newsboy Hat"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat newsboy stars.png",
		description = "<br>[[Starter Pack|Starter Pack DLC]]"
	},
	["Blue Blazer Outfit"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes blazer outfit blue-resources.assets-1925.png"
	},
	["Yellow Leopard Skirt"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes skirt leopard yellow-resources.assets-1719.png"
	},
	["Pink Leopard Skirt"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes skirt leopard pink-resources.assets-1611.png"
	},
	["Desert Goggles"] = {
		class = "Glasses",
		rarity = 2,
		file = "Glasses desert goggles-resources.assets-986.png"
	},
	["Flower Bucket Hat"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat bucket flowers-resources.assets-1612.png"
	},
	["Blue Dainty Hat"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat dainty blue-resources.assets-1203.png"
	},
	["Yellow Leopard Hat"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat leopard yellow-resources.assets-212.png"
	},
	["Pink Leopard Hat"] = {
		class = "Hat",
		rarity = 2,
		file = "Hat leopard pink-resources.assets-622.png"
	},
	["Super Pitbull"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-dog-PitBull.png"
	},
	["Fish Bone"] = {
		class = "MeleeSkin",
		rarity = 2,
		file = "Melee_fishbone-resources.assets-2083.png"
	},
	["Punk Skirt"] = {
		class = "Clothes",
		rarity = 2,
		file = "Clothes_skirt_punk-resources.assets-1962.png"
	},
	["Super Purple Pigeon"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char_pigeon_purple.png"
	},
	["Super Goth Possum"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "Char-opossum-goth.png"
	},
	["Super Gray Goat"] = {
		class = "CharacterSkin",
		rarity = 2
	},
	["Super Green Finch"] = {
		class = "CharacterSkin",
		rarity = 2,
		file = "char_bird_greenfinch.png"
	},
	["Purple Content Creator Hoodie"] = {
		class = "Clothes",
		rarity = 2
	},
	["Wrapped Magnum"] = {
		class = "GunSkin",
		rarity = 2,
		description = "<br>''[[S.A.W Shop]] item''",
		link = "Magnum"
	},
	["Blue Knitted Shotgun"] = {
		class = "GunSkin",
		rarity = 2,
		description = "<br>''[[S.A.W Shop]] item''",
		link = "Shotgun"
	},
	["Red Paper Umbrella"] = {
		class = "Umbrella",
		rarity = 3,
		file = "Umbrella_paper_red-resources.assets-2319.png"
	},
	["Blue Paper Umbrella"] = {
		class = "Umbrella",
		rarity = 3,
		file = "Umbrella_paper_blue-resources.assets-273.png"
	},
	["Brown Paper Umbrella"] = {
		class = "Umbrella",
		rarity = 3,
		file = "Umbrella_paper_brown-resources.assets-1307.png"
	},
	["Green Paper Umbrella"] = {
		class = "Umbrella",
		rarity = 3,
		file = "Umbrella_paper_green-resources.assets-4151.png"
	},
	["Orange Paper Umbrella"] = {
		class = "Umbrella",
		rarity = 3,
		file = "Umbrella_paper_orange-resources.assets-2996.png"
	},
	["Pink Paper Umbrella"] = {
		class = "Umbrella",
		rarity = 3,
		file = "Umbrella_paper_pink-resources.assets-618.png"
	},
	["Yellow Paper Umbrella"] = {
		class = "Umbrella",
		rarity = 3,
		file = "Umbrella_paper_yellow-resources.assets-2306.png"
	},
	["Paw R.I.P. Gravestone"] = {
		class = "Gravestone",
		rarity = 3,
		file = "Gravestone_6-resources.assets-3008.png"
	},
	["Double Paw R.I.P. Gravestone"] = {
		class = "Gravestone",
		rarity = 3,
		file = "Gravestone_7-resources.assets-479.png"
	},
	["Maroon Vigil Gravestone"] = {
		class = "Gravestone",
		rarity = 3,
		file = "Gravestone_8-resources.assets-1633.png"
	},
	["Floss Dance"] = {
		class = "Emote",
		rarity = 3,
		file = "Floss.png"
	},
	["Heel Click"] = {
		class = "Emote",
		rarity = 3,
		file = "Heel_click.png"
	},
	["Hand Stand"] = {
		class = "Emote",
		rarity = 3,
		file = "Handstand.png"
	},
	["Supercharge Fox"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-fox-yellow.png"
	},
	["Super Swamp Skullcat"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-skullcat-sepia.png"
	},
	["Super White Tiger"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-tiger-white.png"
	},
	["Super Shadow Fox"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-fox-purple.png"
	},
	["Super Polar Bear"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-bear-polar.png"
	},
	["Super Shadow Deer"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-deer-black.png"
	},
	["Super Pink Bunny"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-rabbit-pink.png"
	},
	["Super Shadow Raccoon"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-raccoon-dark.png"
	},
	["Super Husky"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-dog-husky.png"
	},
	["Black Evening Dress"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_night_dress_black.png"
	},
	["Black Suit"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_suit_black.png"
	},
	["Popcorn Explosion"] = {
		class = "DeathExplosion",
		rarity = 3,
		file = "Death_popcorn-resources.assets-159.png"
	},
	["Thunderbolt Explosion"] = {
		class = "DeathExplosion",
		rarity = 3,
		file = "Death_thunderbolt-resources.assets-476.png"
	},
	["Emoji (Screaming) Explosion"] = {
		class = "DeathExplosion",
		rarity = 3,
		file = "Death_emoji_icon_scream-resources.assets-668.png"
	},
	["Emoji (Poop) Explosion"] = {
		class = "DeathExplosion",
		rarity = 3,
		file = "Death_emoji_icon_poop-resources.assets-1383.png"
	},
	["Black Tophat"] = {
		class = "Hat",
		rarity = 3,
		file = "Hat_suit_black-resources.assets-290.png"
	},
	["Super Hellfire Skullcat"] = {
		class = "CharacterSkin",
		rarity = 3,
		description = "<br>''[[Super Edition]] reward''"
	},
	["Super Black Panther"] = {
		class = "CharacterSkin",
		rarity = 3
	},
	["Heart Glasses"] = {
		class = "Glasses",
		rarity = 3,
		file = "Glasses_hearts-resources.assets-503.png",
		description = "<br>''[[Founder's Edition]]''"
	},
	["Golden M16"] = {
		class = "GunSkin",
		rarity = 3,
		file = "Gun-m16_golden.png",
		description = "<br>''[[Founder's Edition]]''",
		link = "M16"
	},
	["Golden Silenced Pistol"] = {
		class = "GunSkin",
		rarity = 3,
		file = "Gun-silenced-pistol_golden.png",
		description = "<br>''[[Founder's Edition]]''",
		link = "Silenced Pistol"
	},
	["Charcoal Suit & Pixile Tie"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_suit_grey.png",
		description = "<br>''[[Founder's Edition]]''"
	},
	["Pinstripe Tophat"] = {
		class = "Hat",
		rarity = 3,
		file = "Hat_suit_pinstripe-resources.assets-2360.png"
	},
	["Lumberjack Shirt"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_shirt_lumberjack.png"
	},
	["Super Howler Monkey"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-monkey-white.png"
	},
	["Super Arctic Lion"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-lion-white.png"
	},
	["Super Arctic Wolf"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-wolf-arctic.png"
	},
	Ushanka = {
		class = "Hat",
		rarity = 3,
		file = "Hat_ushanka-resources.assets-2567.png"
	},
	["Super Wiener Dog"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-dog-wiener.png"
	},
	["Super Rudolph Deer"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-deer-reindeer.png"
	},
	["Russian Dance"] = {
		class = "Emote",
		rarity = 3,
		file = "Emote-russian.png"
	},
	["Red Mohawk"] = {
		class = "Hat",
		rarity = 3,
		file = "Hat_mohawk_red-resources.assets-1348.png"
	},
	["Super Arctic Fox"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-fox-arctic.png"
	},
	["Candy Cane"] = {
		class = "MeleeSkin",
		rarity = 3,
		name = "Peppermint Candy Cane",
		file = "Candycane.png"
	},
	["Propeller Hat"] = {
		class = "Hat",
		rarity = 3,
		file = "Hat_propeller-resources.assets-3917.png"
	},
	["Eye Patch"] = {
		class = "Glasses",
		rarity = 3,
		file = "Glasses_patch-resources.assets-1379.png"
	},
	["Pirate Hat"] = {
		class = "Hat",
		rarity = 3,
		file = "Hat_pirate-resources.assets-237.png"
	},
	["Mango Umbrella"] = {
		class = "Umbrella",
		rarity = 3,
		file = "Umbrella_base_mango-resources.assets-4159.png"
	},
	["Pink Jacket"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_jacket_pink-resources.assets-1663.png"
	},
	["Red Spring Dress"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_spring_dress_red-resources.assets-795.png"
	},
	["Super Dark Tiger"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_tiger_negative-resources.assets-3061.png"
	},
	["Super Snowshoe Cat"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_cat_snowshoe-resources.assets-1099.png"
	},
	["Super Shadow Bunny"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char rabbit black-resources.assets-835.png"
	},
	["Super Bubblegum Red Panda"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_redpanda_pink-resources.assets-4189.png"
	},
	["Super Golden Capybara"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_capybara_golden-resources.assets-3651.png"
	},
	["Super Blonde Hyena"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-hyena-tan.png"
	},
	["Super Black Sheep"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-sheep-black.png"
	},
	["Super Dark Sheep"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-sheep-black-invert.png"
	},
	Claymore = {
		class = "MeleeSkin",
		rarity = 3,
		file = "Melee-sword.png"
	},
	Newspaper = {
		class = "MeleeSkin",
		rarity = 3,
		file = "Duck_Newspaper.png"
	},
	["Super Jindo"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-dog-jindo.png"
	},
	["Super Shepweiler"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-dog-odin.png"
	},
	["Super Lioness"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-lion-lioness.png"
	},
	["Super Spotted Pig"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_pig_spotted-resources.assets-3925.png"
	},
	["Super Fennec Fox"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-fox-fire.png"
	},
	["Super Deadly Panda"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_panda_dark-resources.assets-2517.png"
	},
	["Super Twilight Raccoon"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_raccoon_smoky-resources.assets-1296.png"
	},
	["Black Mohawk"] = {
		class = "Hat",
		rarity = 3,
		file = "Hat_mohawk_black-resources.assets-1679.png"
	},
	["Blue Plaid Robe"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_robe_pattern-resources.assets-522.png"
	},
	["Green Striped Robe"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_robe_striped-resources.assets-1709.png"
	},
	["Geologist Hat"] = {
		class = "Hat",
		rarity = 3,
		file = "Hat_geologist-resources.assets-906.png"
	},
	["Super Sparrow"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "char_bird_sparrow-resources.assets-3151.png"
	},
	["Super Red Cardinal"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "char_bird_redcardinal-resources.assets-4168.png"
	},
	["Super Bubblegum Blue Jay"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "char_bluejay_red-resources.assets-4122.png"
	},
	["Super Eagle Owl"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "char_owl_eurasian-resources.assets-2733.png"
	},
	["Super Horned Owl"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "char_owl_horned-resources.assets-2552.png"
	},
	["Super Albino Raven"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "char_raven_albino-resources.assets-1642.png"
	},
	["Larkson Outfit"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_Larkson-resources.assets-2807.png"
	},
	["Lady Caw Caw Outfit"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_dress_CawCaw-resources.assets-2956.png"
	},
	["Blue Jay Z Jacket"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_jacket_BlueJayZ-resources.assets-3040.png"
	},
	["Blue Jay Z Cap"] = {
		class = "Hat",
		rarity = 3,
		file = "Hat_BlueJayZ-resources.assets-1145.png"
	},
	["Lady Caw Caw Hat"] = {
		class = "Hat",
		rarity = 3,
		file = "Hat_CawCaw-resources.assets-1080.png"
	},
	["Lady Caw Caw Glasses"] = {
		class = "Glasses",
		rarity = 3,
		file = "Glasses_CawCaw-resources.assets-574.png"
	},
	["Super White Bunny"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "char_rabbit_white-sharedassets0.assets-128.png"
	},
	["Larkson Glasses"] = {
		class = "Glasses",
		rarity = 3,
		file = "Glasses_Larkson-resources.assets-659.png"
	},
	["Super Greyhound"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "char_dog_greyhound-resources.assets-2738.png"
	},
	["Super Peach Greyhound"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "char_dog_greyhound_tan-resources.assets-4378.png"
	},
	["Pink Onesie"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_onesie_pink-resources.assets-4717.png"
	},
	["Red Onesie"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_onesie_red-resources.assets-588.png"
	},
	["Super Vanilla Skunk"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-skunk-bleached.png"
	},
	["Super Purple Light Sword"] = {
		class = "MeleeSkin",
		rarity = 3,
		file = "Melee_lightsaber_purple-resources.assets-5106.png"
	},
	["Super Sea Otter"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_otter_sea-resources.assets-5023.png"
	},
	["Super Silver Ferret"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_ferret_silver-resources.assets-661.png"
	},
	["Super Duckling"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char duckling-resources.assets-2139.png"
	},
	["Super Eagle"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_hawk_eagle-resources.assets-517.png"
	},
	["Super Speedy Hedgehog"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_hedgehog_blue-resources.assets-908.png"
	},
	["Fish Pajamas"] = {
		class = "Clothes",
		rarity = 3,
		file = "Pajamas_fish.png"
	},
	["Security Guard Outfit"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_securityguard-resources.assets-2127.png"
	},
	["Chartreux Beanie"] = {
		class = "Hat",
		rarity = 3,
		file = "Beanies_catler.png"
	},
	["Steampunk Goggles"] = {
		class = "Hat",
		rarity = 3,
		file = "Steampunk_goggles_foxysongbird.png"
	},
	Harlecorn = {
		class = "Hat",
		rarity = 3,
		file = "Hat_Harle_unicorn_horn.png"
	},
	["Leftstickdown Beanie"] = {
		class = "Hat",
		rarity = 3,
		file = "Beanie_LSD.png"
	},
	["Shirakami Scarf"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_scarf_shirakami.png"
	},
	["Lyze's Key"] = {
		class = "MeleeSkin",
		rarity = 3,
		file = "Melee_key-resources.assets-1437.png"
	},
	["Duck Newspaper"] = {
		class = "MeleeSkin",
		rarity = 3,
		file = "Duck_Newspaper.png"
	},
	["Pro Gamer Hoodie"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_hoodie_pink_full.png"
	},
	["Super Mouse"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_rat_mouse-resources.assets-3797.png"
	},
	["Super White Bat"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_bat_white-resources.assets-3662.png"
	},
	["Banan Hat"] = {
		class = "Hat",
		rarity = 3,
		file = "Hat_banana_peel.png"
	},
	["Super Pomeranian"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_dog_pomeranian-resources.assets-5080.png"
	},
	["Paw Star Tank Top"] = {
		class = "Clothes",
		rarity = 3,
		file = "Paw_Star_Tank_Top.png"
	},
	["Pink Lei Hula Skirt"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_bikini_hula.png"
	},
	["Carrot Onesie"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_onesie_carrot-resources.assets-4647.png"
	},
	["Pro Gamer Hat"] = {
		class = "Hat",
		rarity = 3,
		file = "Hat_newsboy_pink-resources.assets-5283.png"
	},
	["Super Fruit Bat"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_bat_fruit-resources.assets-4605.png"
	},
	["Super Twilight Fox"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_fox_dark-resources.assets-5322.png"
	},
	["Super Berry Gummy Bear"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_bear_gummy_blue-resources.assets-3422.png"
	},
	["Super Lime Gummy Bear"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_bear_gummy_green-resources.assets-1239.png"
	},
	["Super Orange Gummy Bear"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_bear_gummy_orange-resources.assets-675.png"
	},
	["Super Cherry Gummy Bear"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_bear_gummy_red-resources.assets-1880.png"
	},
	["Super Bubblegum Skullcat"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_skullcat_pink-resources.assets-2457.png"
	},
	["Super Crank Raccoon"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Crank Raccoon.png"
	},
	["Super Iced Sloth"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_sloth_ice-resources.assets-4692.png"
	},
	["Super Peach Sloth"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_sloth_peach-resources.assets-305.png"
	},
	["Super Persian Cat"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_cat_persian-resources.assets-5019.png"
	},
	["Super Scottish Fold"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_cat_scottish_fold-resources.assets-2903.png"
	},
	["Super Border Collie"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-dog-border-collie.png"
	},
	["Super Flaming Wolf"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_wolf_fire-resources.assets-568.png"
	},
	["Super Blue Steel Hyena"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_hyena_striped_blue-resources.assets-5471.png"
	},
	["Super Light Striped Hyena"] = {
		class = "CharacterSkin",
		rarity = 3
	},
	["Super Babirusa"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_boar_babirusa-resources.assets-4572.png"
	},
	["Super Boarlet"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_boar_baby-resources.assets-4763.png"
	},
	["Super Pine Marten"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_ferret_marten-resources.assets-171.png"
	},
	["Super Frozen Skullcat"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_skullcat_ice-resources.assets-3331.png"
	},
	["Graduate Hat"] = {
		class = "Hat",
		rarity = 3,
		file = "Hat_graduate.png"
	},
	["RedShift Outfit"] = {
		class = "Clothes",
		rarity = 3,
		file = "RedShift_Outfit.png"
	},
	["Super Rooster"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_chicken_rooster-resources.assets-3273.png"
	},
	["Super Bull"] = {
		class = "Character",
		rarity = 3,
		file = "Char_cow_bull-resources.assets-3157.png"
	},
	["Super Pinto Horse"] = {
		class = "Character",
		rarity = 3,
		file = "Char_horse_pinto-resources.assets-2938.png"
	},
	["Tap Dance"] = {
		class = "Emote",
		rarity = 3,
		file = "Tapdance.png"
	},
	["Super Cockatoo"] = {
		class = "Character",
		rarity = 3,
		file = "Char_parrot_cockatoo-resources.assets-1322.png"
	},
	["Banana Costume"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_banana-resources.assets-1461.png"
	},
	["Pizza Slice"] = {
		class = "Hat",
		rarity = 3,
		file = "Hat_pizzaslice.png"
	},
	Plumbrella = {
		class = "Umbrella",
		rarity = 3,
		file = "Umbrella_plumbrella.png"
	},
	["Candlelit Gravestone"] = {
		class = "Gravestone",
		rarity = 3,
		file = "Gravestone_candles-resources.assets-251.png"
	},
	["Pizza Box Gravestone"] = {
		class = "Gravestone",
		rarity = 3,
		file = "Gravestone_pizzabox-resources.assets-591.png"
	},
	["Teal Retro Tracksuit"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_tracksuit_purple-resources.assets-748.png"
	},
	["Pink Retro Tracksuit"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_tracksuit_pink-resources.assets-762.png"
	},
	["Super Bubblegum Leopard"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_leopard_bubblegum-resources.assets-1489.png"
	},
	["Super Macaroni Penguin"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_penguin_eyebrows-resources.assets-590.png"
	},
	["Blue Down Jacket"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_down_jacket_blue-resources.assets-1106.png"
	},
	["Pink Down Jacket"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_down_jacket_pink-resources.assets-901.png"
	},
	["White Down Jacket"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_down_jacket_white.png"
	},
	["Wreath Gravestone"] = {
		class = "Gravestone",
		rarity = 3,
		file = "Gravestone_wreath-resources.assets-756.png"
	},
	["Super Pixile Tiger"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_tiger_purple-resources.assets-1548.png"
	},
	Earbud = {
		class = "MeleeSkin",
		rarity = 3,
		file = "Melee_earpiece.png"
	},
	["Super Lunar Rat"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_rat_lunar-resources.assets-1307.png"
	},
	["Super Field Mouse"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_rat_mouse_field-resources.assets-1228.png"
	},
	Astropack = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_backpack_spaceship-resources.assets-1360.png"
	},
	["Super Chick"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-chicken-chick.png"
	},
	["Super Coyote"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-wolf-coyote.png"
	},
	["Super Sphinx"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-cat-sphinx.png"
	},
	["Ankh Staff"] = {
		class = "MeleeSkin",
		rarity = 3,
		file = "Melee_ankh.png"
	},
	["Atef Crown"] = {
		class = "Hat",
		rarity = 3,
		file = "Hat_atef_crown.png"
	},
	["Hieroglyph Umbrella"] = {
		class = "Umbrella",
		rarity = 3,
		file = "Umbrella_hieroglyph.png"
	},
	["White Pharaoh Costume"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_pharaoh_white.png"
	},
	["Black Triangle Shades"] = {
		class = "Glasses",
		rarity = 3,
		file = "Glasses_triangle_black-resources.assets-420.png",
		description = "<br>''[[Super Edition]] reward''"
	},
	["School Uniform (Boy)"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_uniform_boy-resources.assets-1973.png",
		description = "<br>''[[Super Edition]] reward''"
	},
	["3D Glasses"] = {
		class = "Glasses",
		rarity = 3,
		file = "Glasses_3D-resources.assets-239.png",
		description = "<br>''[[Super Edition]] reward''"
	},
	["School Uniform (Girl)"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_uniform_girl-resources.assets-473.png",
		description = "<br>''[[Super Edition]] reward''"
	},
	["Super Hellfire Fox"] = {
		class = "CharacterSkin",
		rarity = 3
	},
	["Super Splash Bear"] = {
		class = "CharacterSkin",
		rarity = 3,
		description = "<br>''[[Super Edition]] reward''"
	},
	["Super Leaf Skullcat"] = {
		class = "CharacterSkin",
		rarity = 3,
		description = "<br>''[[Super Edition]] reward''"
	},
	["Super Thunder Tiger"] = {
		class = "CharacterSkin",
		rarity = 3,
		description = "<br>''[[Super Edition]] reward''"
	},
	["Rainbow Dress"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_dress_rainbow.png"
	},
	["Blue Hoodie & Camo Pants"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes_cait.png"
	},
	["Rebellion AK"] = {
		class = "GunSkin",
		rarity = 3,
		file = "Gun-ak_rebellion.png",
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''",
		link = "AK"
	},
	["Eagle Deagle"] = {
		class = "GunSkin",
		rarity = 3,
		file = "Gun deagle eagle-resources.assets-433.png",
		description = "<br>''[[S.A.W Shop]] item''",
		link = "Deagle"
	},
	["SAW M16"] = {
		class = "GunSkin",
		rarity = 3,
		file = "Gun-m16_SAW.png",
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''",
		link = "M16"
	},
	["Pineapple Minigun"] = {
		class = "GunSkin",
		rarity = 3,
		file = "Gun minigun pineapple-resources.assets-373.png",
		description = "<br>''[[S.A.W Shop]] item''",
		link = "Minigun"
	},
	["Elite Silenced Pistol"] = {
		class = "GunSkin",
		rarity = 3,
		file = "Elite_Silenced_Pistol.png",
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''",
		link = "Silenced Pistol"
	},
	["Hellfire SMG"] = {
		class = "GunSkin",
		rarity = 3,
		file = "Gun-smg_fire.png",
		description = "<br>''[[The Great Gun Buyback|The Great Gun Buyback]] reward''",
		link = "SMG"
	},
	["Camo Sniper"] = {
		class = "GunSkin",
		rarity = 3,
		file = "Gun_sniper_camo-resources.assets-1279.png",
		description = "<br>''[[S.A.W Shop]] item''",
		link = "Sniper"
	},
	["Hoodie (Blue)"] = {
		class = "Clothes",
		rarity = 3
	},
	["Hoodie (Green)"] = {
		class = "Clothes",
		rarity = 3
	},
	["White AK"] = {
		class = "GunSkin",
		rarity = 3,
		group = 8,
		description = "<br>500 AK kills",
		link = "AK"
	},
	["White Bow & Sparrow"] = {
		class = "GunSkin",
		rarity = 3,
		file = "White_Bow_&_Sparrow.png",
		group = 8,
		description = "<br>100 Bow & Sparrow kills",
		link = "Bow & Sparrow"
	},
	["White Sparrow Launcher"] = {
		class = "GunSkin",
		rarity = 3,
		file = "White_Sparrow_Launcher.png",
		group = 8,
		description = "<br>100 Sparrow Launcher kills",
		link = "Sparrow Launcher"
	},
	["White Poison Dart Gun"] = {
		class = "GunSkin",
		rarity = 3,
		name = "White Dogna's Dart Gun",
		group = 8,
		description = "<br>150 Poison Dart Gun kills",
		link = "Dogna's Dart Gun"
	},
	["White Deagle"] = {
		class = "GunSkin",
		rarity = 3,
		group = 8,
		description = "<br>200 Deagle kills",
		link = "Deagle"
	},
	["White Hunting Rifle"] = {
		class = "GunSkin",
		rarity = 3,
		file = "Gun-rifle_white.png",
		group = 8,
		description = "<br>100 Hunting Rifle kills",
		link = "Hunting Rifle"
	},
	["White JAG-7"] = {
		class = "GunSkin",
		rarity = 3,
		group = 8,
		description = "<br>250 JAG-7 kills",
		link = "JAG-7"
	},
	["White M16"] = {
		class = "GunSkin",
		rarity = 3,
		group = 8,
		description = "<br>250 M16 kills",
		link = "M16"
	},
	["White Magnum"] = {
		class = "GunSkin",
		rarity = 3,
		group = 8,
		description = "<br>150 Magnum kills",
		link = "Magnum"
	},
	["White Minigun"] = {
		class = "GunSkin",
		rarity = 3,
		group = 8,
		description = "<br>100 Minigun kills",
		link = "Minigun"
	},
	["White Pistol"] = {
		class = "GunSkin",
		rarity = 3,
		group = 8,
		description = "<br>100 Pistol kills",
		link = "Pistol"
	},
	["White SMG"] = {
		class = "GunSkin",
		rarity = 3,
		group = 8,
		description = "<br>500 SMG kills",
		link = "SMG"
	},
	["White Shotgun"] = {
		class = "GunSkin",
		rarity = 3,
		group = 8,
		description = "<br>500 Shotgun kills",
		link = "Shotgun"
	},
	["White Silenced Pistol"] = {
		class = "GunSkin",
		rarity = 3,
		group = 8,
		description = "<br>200 Silenced Pistol kills",
		link = "Silenced Pistol"
	},
	["White Sniper"] = {
		class = "GunSkin",
		rarity = 3,
		group = 8,
		description = "<br>100 Sniper kills",
		link = "Sniper"
	},
	["White Thomas Gun"] = {
		class = "GunSkin",
		rarity = 3,
		group = 8,
		description = "<br>250 Thomas Gun kills",
		link = "Thomas Gun"
	},
	["Black Bicorne"] = {
		class = "Hat",
		rarity = 3
	},
	["Camo (Brown)"] = {
		class = "Clothes",
		rarity = 3
	},
	["Camo (Green)"] = {
		class = "Clothes",
		rarity = 3
	},
	["Camo (Gray)"] = {
		class = "Clothes",
		rarity = 3
	},
	["Ghillie Suit"] = {
		class = "Clothes",
		rarity = 3
	},
	["Professor Outfit (Tweed)"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes professor blazer tweed-resources.assets-1236.png"
	},
	["Purple Triangle Shades"] = {
		class = "Glasses",
		rarity = 3,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	Pitchfork = {
		class = "MeleeSkin",
		rarity = 3,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	["Grapefruit Umbrella"] = {
		class = "Umbrella",
		rarity = 3
	},
	["Lemon Fruit Umbrella"] = {
		class = "Umbrella",
		rarity = 3,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	["Lime Fruit Umbrella"] = {
		class = "Umbrella",
		rarity = 3,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	["Scuba Outfit"] = {
		class = "Clothes",
		rarity = 3,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	["Newspaper Hat"] = {
		class = "Hat",
		rarity = 3,
		file = "Hat_paperhat_newspaper-resources.assets-729.png"
	},
	["Professor Outfit (Gray)"] = {
		class = "Clothes",
		rarity = 3,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	["Orange Fruit Umbrella"] = {
		class = "Umbrella",
		rarity = 3,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	["Pilot Hat"] = {
		class = "Hat",
		rarity = 3,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	["Yellow Sweater Skirt"] = {
		class = "Clothes",
		rarity = 3,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	["Selfie Stick"] = {
		class = "MeleeSkin",
		rarity = 3,
		file = "Melee selfiestick-resources.assets-921.png"
	},
	["Yellow Triangle Shades"] = {
		class = "Glasses",
		rarity = 3,
		file = "Glasses triangle yellow-resources.assets-1820.png"
	},
	["Red Triangle Shades"] = {
		class = "Glasses",
		rarity = 3,
		file = "Glasses triangle red-resources.assets-616.png"
	},
	["Candy Corn Ferret"] = {
		class = "CharacterSkin",
		rarity = 3,
		name = "Super Candy Corn Ferret",
		file = "Char-Ferret-candycorn.png"
	},
	["Dracula Parrot"] = {
		class = "Character",
		rarity = 3,
		name = "Super Dracula Parrot",
		file = "Char-parrot-dracula.png"
	},
	["Super Twilight Fennec Fox"] = {
		class = "CharacterSkin",
		rarity = 3,
		description = "<br>[[Starter Pack|Starter Pack DLC]]"
	},
	["Tartan Schoolgirl Outfit"] = {
		class = "Clothes",
		rarity = 3,
		file = "Clothes schoolgirl tartan-resources.assets-897.png"
	},
	Flamberge = {
		class = "MeleeSkin",
		rarity = 3
	},
	["Super Samoyed"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-dog-Samoyed.png"
	},
	["Super German Shepherd"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char-dog-GermanShepherd.png"
	},
	["Blue Mohawk"] = {
		class = "Hat",
		rarity = 3,
		file = "Hat_mohawk_blue-resources.assets-483.png"
	},
	["Green Mohawk"] = {
		class = "Hat",
		rarity = 3
	},
	["Purple Mohawk"] = {
		class = "Hat",
		rarity = 3,
		file = "Hat mohawk purple-resources.assets-1978.png"
	},
	["Super Satinette Pigeon"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_pigeon_satinette.png"
	},
	["Super Dove"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "Char_pigeon_dove.png"
	},
	["Super Devil Goat"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "char_goat_red.png"
	},
	["Super Chamois Goat"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "char_goat_chamois.png"
	},
	["Super Four-Horned Goat"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "char_goat_4horn.png"
	},
	["Super Peppermint Hyena"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "char_hyena_striped_peppermint.png"
	},
	["Super Knitted Sheep"] = {
		class = "CharacterSkin",
		rarity = 3,
		file = "char_sheep_knitted.png"
	},
	["Snowflake Pinwheel"] = {
		class = "MeleeSkin",
		rarity = 3
	},
	["CRISPRmas Tree Umbrella"] = {
		class = "Umbrella",
		rarity = 3,
		description = "<br>''[[S.A.W Shop]] item''"
	},
	["Bear Umbrella"] = {
		class = "Umbrella",
		rarity = 4,
		file = "Umbrella_animal_bear-resources.assets-2310.png"
	},
	["Deer Umbrella"] = {
		class = "Umbrella",
		rarity = 4,
		file = "Umbrella_animal_deer-resources.assets-2388.png"
	},
	["Fox Umbrella"] = {
		class = "Umbrella",
		rarity = 4,
		file = "Umbrella_animal_fox-resources.assets-3886.png"
	},
	["Panda Umbrella"] = {
		class = "Umbrella",
		rarity = 4,
		file = "Umbrella_animal_panda-resources.assets-2225.png"
	},
	["Raccoon Umbrella"] = {
		class = "Umbrella",
		rarity = 4,
		file = "Umbrella_animal_raccoon-resources.assets-1120.png"
	},
	["Skullcat Umbrella"] = {
		class = "Umbrella",
		rarity = 4,
		file = "Umbrella_animal_skullcat-resources.assets-3357.png"
	},
	["Tiger Umbrella"] = {
		class = "Umbrella",
		rarity = 4,
		file = "Umbrella_animal_tiger-resources.assets-1352.png"
	},
	["Super Metal Panda"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char-panda-metal.png"
	},
	["Spaghetti Explosion"] = {
		class = "DeathExplosion",
		rarity = 4,
		file = "Death_spaghetti-resources.assets-3013.png"
	},
	["Twister Explosion"] = {
		class = "DeathExplosion",
		rarity = 4,
		file = "Death_twister-resources.assets-589.png"
	},
	["Fireworks Explosion"] = {
		class = "DeathExplosion",
		rarity = 4,
		file = "Death_fireworks-resources.assets-3613.png"
	},
	["Juggling Roulette"] = {
		class = "Emote",
		rarity = 4,
		file = "Juggle.png",
		description = "<br>''[[Founder's Edition]]''"
	},
	Halo = {
		class = "Hat",
		rarity = 4,
		file = "Hat_halo-resources.assets-1101.png",
		description = "<br>''[[Founder's Edition]]''"
	},
	["Pink Princess Dress"] = {
		class = "Clothes",
		rarity = 4,
		file = "Clothes_princess_pink.png"
	},
	["Super Cotton Candy Deer"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char-deer-cotton-candy.png"
	},
	["Super Corgi"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char-dog-corgi.png"
	},
	["Hole in One!"] = {
		class = "Emote",
		rarity = 4,
		file = "Hole_in_one.png"
	},
	["Khaki Trenchcoat"] = {
		class = "Clothes",
		rarity = 4,
		file = "Clothes_trenchcoat_khaki.png"
	},
	["Black Pirate Costume"] = {
		class = "Clothes",
		rarity = 4,
		file = "Clothes_pirate_black.png"
	},
	["Blue Pirate Costume"] = {
		class = "Clothes",
		rarity = 4,
		file = "Clothes_pirate_blue.png"
	},
	["Brown Pirate Costume"] = {
		class = "Clothes",
		rarity = 4,
		file = "Clothes_pirate_brown.png"
	},
	["Green Pirate Costume"] = {
		class = "Clothes",
		rarity = 4,
		file = "Clothes_pirate_green.png"
	},
	["Super Bear Beanie"] = {
		class = "Hat",
		rarity = 4,
		file = "Hat_beanies_bear-resources.assets-3450.png"
	},
	["Super Deer Beanie"] = {
		class = "Hat",
		rarity = 4,
		file = "Hat_beanies_deer-resources.assets-2655.png"
	},
	["Super Fox Beanie"] = {
		class = "Hat",
		rarity = 4,
		file = "Hat_beanies_fox-resources.assets-3303.png"
	},
	["Super Panda Beanie"] = {
		class = "Hat",
		rarity = 4,
		file = "Hat_beanies_panda-resources.assets-3139.png"
	},
	["Super Raccoon Beanie"] = {
		class = "Hat",
		rarity = 4,
		file = "Hat_beanies_raccoon-resources.assets-2978.png"
	},
	["Super Skullcat Beanie"] = {
		class = "Hat",
		rarity = 4,
		file = "Hat_beanies_skullcat-resources.assets-3451.png"
	},
	["Super Tiger Beanie"] = {
		class = "Hat",
		rarity = 4,
		file = "Hat_beanies_tiger-resources.assets-3072.png"
	},
	["Super Hot Tiger"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char-tiger-jaguar.png"
	},
	["Super Tanuki"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char_raccoon_tanuki-resources.assets-2967.png"
	},
	["Super Golden Pig"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char-pig-golden.png"
	},
	["Super Maned Wolf"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char-wolf-maned.png"
	},
	["Super Zebra"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char-donkey-zebra.png"
	},
	["Geologist Outfit"] = {
		class = "Clothes",
		rarity = 4,
		file = "Clothes_geologist-resources.assets-629.png"
	},
	["Pink Moustache"] = {
		class = "Beard",
		rarity = 4,
		file = "Moustache6_pink-resources.assets-2866.png"
	},
	["Purple Jacket"] = {
		class = "Clothes",
		rarity = 4,
		file = "Clothes_jacket_purple-resources.assets-4237.png"
	},
	["Super Chocolate Bunny"] = {
		class = "CharacterSkin",
		rarity = 4
	},
	["Super Gold Light Sword"] = {
		class = "MeleeSkin",
		rarity = 4,
		file = "Melee_lightsaber_orange-resources.assets-3457.png"
	},
	["Super Red Light Sword"] = {
		class = "MeleeSkin",
		rarity = 4,
		file = "Melee_lightsaber_red-resources.assets-3543.png"
	},
	["Black Princess Dress"] = {
		class = "Clothes",
		rarity = 4,
		file = "Clothes_princess_black.png"
	},
	["Blue Princess Dress"] = {
		class = "Clothes",
		rarity = 4,
		file = "Clothes_princess_blue.png"
	},
	["Green Princess Dress"] = {
		class = "Clothes",
		rarity = 4,
		file = "Clothes_princess_green.png"
	},
	["Purple Princess Dress"] = {
		class = "Clothes",
		rarity = 4,
		file = "Clothes_princess_purple.png"
	},
	["Yellow Princess Dress"] = {
		class = "Clothes",
		rarity = 4,
		file = "Clothes_princess_yellow.png"
	},
	["Donk's Damaged Suit"] = {
		class = "Clothes",
		rarity = 4
	},
	["Howl's Suit"] = {
		class = "Clothes",
		rarity = 4
	},
	["Super Berry Blue Owl"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char_owl_blue-resources.assets-1438.png"
	},
	["Super Westie"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char_dog_westie-resources.assets-1956.png"
	},
	["Carrot Gravestone"] = {
		class = "Gravestone",
		rarity = 4,
		file = "Gravestone_9-resources.assets-1831.png"
	},
	["Super Moose"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char-deer-moose.png"
	},
	["Super Two-toed Sloth"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char_sloth_twotoed-resources.assets-1154.png"
	},
	["Super Fire Striped Hyena"] = {
		class = "CharacterSkin",
		rarity = 4
	},
	["Super Ram"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char_sheep_ram-resources.assets-5102.png"
	},
	["Super Flaming Boar"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char_boar_fire-resources.assets-512.png"
	},
	["Super Tricolor Corgi"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char_dog_corgi_tricolor-resources.assets-2656.png"
	},
	["Super Fried Chicken"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char_chicken_fried-resources.assets-220.png"
	},
	["Super Lynx"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "char-cat-lynx.png"
	},
	["Super Bobcat"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "char-cat-bobcat.png"
	},
	["Super Skelecat"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char-skullcat-mossy.png"
	},
	["Super Alebrije Tiger"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char_tiger_alebrijes-resources.assets-333.png"
	},
	["Super Alebrije Blue Jay"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char_bluejay_alebrijes-resources.assets-1068.png"
	},
	["Super Alebrije Monkey"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char_monkey_alebrijes-resources.assets-1442.png"
	},
	["Super Pumpkin Panda"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char_panda_pumpkin-resources.assets-1118.png"
	},
	["Super Alebrije Otter"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char_otter_alebrijes-resources.assets-460.png"
	},
	["Super Werewolf"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char_wolf_werewolf-resources.assets-1628.png"
	},
	["Super Vampire Bat"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char_bat_vampire-resources.assets-1418.png"
	},
	["Super Scarecrow"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char raven scarecrow-resources.assets-609.png"
	},
	["Super Turkey"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Turkey.png"
	},
	["Super Snow Leopard"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char_leopard_snow.png"
	},
	["Super Gingerbread Bear"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char-bear-water.png"
	},
	["Super Golden Mouse"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char_rat_mouse_golden-resources.assets-596.png"
	},
	["Super Toy Mouse"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char_rat_mouse_toy-resources.assets-698.png"
	},
	["Super Lucky Lion"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char lion clover-resources.assets-1310.png"
	},
	["Super Roadrunner"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char-bluejay-roadrunner.png"
	},
	["Super Bastet Cat"] = {
		class = "CharacterSkin",
		rarity = 4,
		name = "Super Bastet",
		file = "Char-cat-bastet.png"
	},
	["Super Anubis"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char-wolf-anubis.png"
	},
	["Super Horus Falcon"] = {
		class = "CharacterSkin",
		rarity = 4,
		name = "Super Horus",
		file = "Char-hawk-horus.png"
	},
	Khopesh = {
		class = "MeleeSkin",
		rarity = 4,
		file = "Melee_khopesh.png"
	},
	["Crown of Hathor"] = {
		class = "Hat",
		rarity = 4,
		file = "Hat_hathor_crown.png"
	},
	["Red Pharaoh Costume"] = {
		class = "Clothes",
		rarity = 4,
		file = "Clothes_pharaoh_red.png"
	},
	["Maid Outfit"] = {
		class = "Clothes",
		rarity = 4,
		file = "Clothes_maid_dress-resources.assets-597.png",
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward"
	},
	Swordfish = {
		class = "MeleeSkin",
		rarity = 4,
		file = "Melee_swordfish-resources.assets-544.png",
		description = "<br>''[[Super Edition]] reward''"
	},
	Lightbulb = {
		class = "Hat",
		rarity = 4,
		file = "Hat_lightbulb-resources.assets-205.png",
		description = "<br>''[[Super Edition]] reward''"
	},
	["SARturday Night Fever"] = {
		class = "Emote",
		rarity = 4,
		description = "<br>''[[Super Edition]] reward''"
	},
	["Super Pineapple Hedgehog"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char-hedgehog-pineapple.png"
	},
	["Super Watermelon Skunk"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "Char-skunk-watermelon.png"
	},
	["Sakura Sniper"] = {
		class = "GunSkin",
		rarity = 4,
		file = "Gun-sniper_sakura.png",
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''",
		link = "Sniper"
	},
	["Super Cat Beanie"] = {
		class = "Hat",
		rarity = 4
	},
	["Beauty Sloth Beanie"] = {
		class = "Hat",
		rarity = 4
	},
	["Gray Trenchcoat"] = {
		class = "Clothes",
		rarity = 4,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	["Scuba Goggles"] = {
		class = "Glasses",
		rarity = 4,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	["I Surrender!"] = {
		class = "Emote",
		rarity = 4,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	["Stacked Hats"] = {
		class = "Hat",
		rarity = 4,
		description = "<br>''[[Super Animal Pass Season 0|Super Animal Pass Season 0]] reward''"
	},
	["Hypnosis Glasses"] = {
		class = "Glasses",
		rarity = 4,
		file = "Glasses pinwheel animated-resources.assets-1379.png"
	},
	["Zombie Tiger"] = {
		class = "CharacterSkin",
		rarity = 4,
		name = "Super Zombie Tiger",
		file = "Char-Tiger-zombie.png"
	},
	["Super Lionhead Rabbit"] = {
		class = "CharacterSkin",
		rarity = 4,
		file = "char_rabbit_lionhead.png"
	},
	["Blue Tie"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tshirt_blue.png"
	},
	["Emoji (Rock & Roll) Explosion"] = {
		class = "DeathExplosion",
		rarity = 5,
		file = "Rock_and_roll_death_explosion.png"
	},
	["Beta Blue Beanie"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_beanie_blue-resources.assets-2151.png",
		description = "<br>''Beta Tester exclusive''"
	},
	["Military Helmet"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_helmet_military-resources.assets-3622.png"
	},
	["Beta Tee"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tshirt_beta.png",
		description = "<br>''Beta Tester exclusive''"
	},
	["!Keyme Tee"] = {
		class = "Clothes",
		rarity = 5,
		name = "!keyme Tee",
		file = "Clothes_tshirt_key.png",
		description = "<br>''Beta Tester exclusive''"
	},
	["Green Tie"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tshirt_green.png"
	},
	["Orange Tie"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tie_orange.png"
	},
	["White Tie"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tie_white.png"
	},
	["SAW Helmet"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_helmet_SAW-resources.assets-3265.png"
	},
	["Golden Video Jacket"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_jacket_golden.png",
		description = "<br>''SAR Tonight Winner Reward''"
	},
	["Santa Outfit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_santa.png"
	},
	["Festive Sweater"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_sweater_christmas.png"
	},
	["Santa Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_santa-resources.assets-1805.png"
	},
	["Banana Tee"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tshirt_banana.png"
	},
	["Banana Fancy Shirt"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_shirt_banana.png"
	},
	["Lab Coat"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_labcoat.png"
	},
	["SAR Tonight Cap"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_baseball_SARtonight-resources.assets-2626.png",
		description = "<br>''Winner Reward''"
	},
	["Howl Tee"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tshirt_print_Howl.png",
		description = "<br>''SAR Tonight Winner Reward''"
	},
	["Donk Tee"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tshirt_print_Donk.png",
		description = "<br>''SAR Tonight Winner Reward''"
	},
	["Tiger Silhouette Tee"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tshirt_silhouette_tiger.png",
		description = "<br>''All Participants of Ep. 1/2 Get''"
	},
	["Fox Silhouette Tee"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tshirt_silhouette_fox-resources.assets-4489.png",
		description = "<br>''All Winners of Ep. 3 Get''"
	},
	["SAR Tonight Tee"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tshirt_SARtonight_logo.png",
		description = "<br>''Winner Reward''"
	},
	["Small Crown"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_crown_small-resources.assets-995.png"
	},
	Crown = {
		class = "Hat",
		rarity = 5,
		file = "Hat_crown-resources.assets-1055.png"
	},
	["Rebellion Helmet"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_helmet_rebellion-resources.assets-468.png"
	},
	["Safari Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_safari-resources.assets-945.png"
	},
	["Green Bow Tie"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_bowtie_green-resources.assets-691.png"
	},
	["Blue Bow Tie"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_bowtie_blue-resources.assets-3333.png"
	},
	["Orange Bow Tie"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_bowtie_orange-resources.assets-695.png"
	},
	["Purple Bow Tie"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_bowtie_purple-resources.assets-2224.png"
	},
	["Maroon Bow Tie"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_bowtie_maroon-resources.assets-469.png"
	},
	["White Bow Tie"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_bowtie_white-resources.assets-3302.png"
	},
	["Black Bow Tie"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_bowtie_black-resources.assets-2420.png"
	},
	["Purple Tie"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tie_purple-resources.assets-522.png"
	},
	["Maroon Tie"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tie_maroon-resources.assets-588.png"
	},
	["Black Tie"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tie_black-resources.assets-583.png"
	},
	["Red Eye Scope"] = {
		class = "Glasses",
		rarity = 5,
		file = "Glasses_terminator-resources.assets-350.png"
	},
	Hatchet = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_hatchet-resources.assets-1429.png"
	},
	Axe = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_axe-resources.assets-1159.png"
	},
	Tomahawk = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_tomahawk-resources.assets-3574.png"
	},
	["Rebellion Sword"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee-broadsword.png"
	},
	Ruler = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_ruler-resources.assets-1331.png",
		description = "<br>''Admin/Moderator exclusive item''"
	},
	Crowbar = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee-crowbar.png"
	},
	["Marshmallow Stick"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee-marshmallow_white.png"
	},
	["Cooked Marshmallow Stick"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee-marshmallow_cooked.png"
	},
	["Burnt Marshmallow Stick"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee-marshmallow_burnt.png"
	},
	Sickle = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee-sickle.png"
	},
	["Large Sickle"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee-sickle_asian.png"
	},
	Scythe = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee-scythe.png"
	},
	Shovel = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee-shovel.png"
	},
	["Swag Glasses"] = {
		class = "Glasses",
		rarity = 5,
		file = "Glasses_swag.png"
	},
	["Palm Shirt"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_shirt_palm.png"
	},
	["Coconut Hula Skirt"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_bikini_coconut.png"
	},
	["Grenade Tee"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tshirt_grenade.png"
	},
	["Coconut Tee"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tshirt_coconut.png"
	},
	["Tai Chi Sword"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee-taichi_sword.png",
		description = "<br>''[[Year of the Super Pig]]''"
	},
	["Bamboo Stick"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee-bamboo.png",
		description = "<br>''[[Year of the Super Pig]]''"
	},
	["Lunar Black Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_chinese_black.png",
		description = "<br>''[[Year of the Super Pig]]''"
	},
	["Lunar Red Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_chinese_red.png",
		description = "<br>''[[Year of the Super Pig]]''"
	},
	["Lunar Shirt"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_shirt_chinese.png",
		description = "<br>''[[Year of the Super Pig]]''"
	},
	["Lunar Dress"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_dress_chinese.png",
		description = "<br>''[[Year of the Super Pig]]''"
	},
	["Lunar Traditional Outfit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_outfit_chinese.png",
		description = "<br>''[[Year of the Super Pig]]''"
	},
	["Lunar Vest"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_vest_chinese.png",
		description = "<br>''[[Year of the Super Pig]]''"
	},
	["Duos Day Rose"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_rose-resources.assets-4153.png"
	},
	Paintbrush = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_paintbrush-resources.assets-4056.png",
		description = "<br>''Super Artist exclusive item''"
	},
	Pen = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_pen-resources.assets-3548.png",
		description = "<br>''Translator exclusive item''"
	},
	Pencil = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_pencil.png"
	},
	["Super Tape Jacket"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_jacket_tape-resources.assets-4118.png"
	},
	["Crate Tee"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tshirt_crate-resources.assets-772.png"
	},
	Rags = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_rags-resources.assets-4707.png"
	},
	["Health Juice Tee"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tshirt_juice-resources.assets-4586.png"
	},
	["Super Tape Tee"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tshirt_tape-resources.assets-1606.png"
	},
	["Red Pinwheel Glasses"] = {
		class = "Glasses",
		rarity = 5,
		file = "Glasses_pinwheel_red-resources.assets-3650.png"
	},
	["Green Pinwheel Glasses"] = {
		class = "Glasses",
		rarity = 5,
		file = "Glasses_pinwheel_green-resources.assets-4865.png"
	},
	Pinwheel = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_pinwheel-resources.assets-1964.png"
	},
	["Camo Helmet"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_helmet_camo-resources.assets-3326.png"
	},
	["Juice Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_juice_hat-resources.assets-3713.png"
	},
	["Medic Helmet"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_helmet_medic-resources.assets-504.png"
	},
	["Purple Robe"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_robe_twitch-resources.assets-343.png",
		description = "<br>''Twitch Content Creator exclusive item''"
	},
	["Red Robe"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_robe_youtube-resources.assets-2320.png",
		description = "<br>''YouTube Content Creator exclusive item''"
	},
	["Lucky Top Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_tophat_green-resources.assets-2399.png"
	},
	["Lucky Suit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_suit_green-resources.assets-4778.png"
	},
	["Lucky Beard"] = {
		class = "Beard",
		rarity = 5,
		file = "Beard6_red-resources.assets-122.png"
	},
	["Star Glasses"] = {
		class = "Glasses",
		rarity = 5,
		file = "Glasses_stars-resources.assets-254.png",
		description = "<br>''Content Creator exclusive item''"
	},
	Banana = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_banana-resources.assets-4256.png",
		description = "<br>''[[April Fools' Day]]''"
	},
	["SAR Tonight Umbrella"] = {
		class = "Umbrella",
		rarity = 5,
		file = "Umbrella_base_SARtonight-resources.assets-2346.png",
		description = "<br>''Winner Reward''"
	},
	["SAR Tonight Microphone"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_reporter-resources.assets-4426.png",
		description = "<br>''Winner Reward''"
	},
	["Donk's Suit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_suit_navy-resources.assets-3673.png"
	},
	["Bunny Ears"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_bunnyears-sharedassets0.assets-46.png"
	},
	["Blue Bow"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_bow_blue-resources.assets-1537.png"
	},
	["Yellow Bow"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_bow_yellow-sharedassets0.assets-19.png"
	},
	["Pink Bow"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_bow_pink-resources.assets-843.png"
	},
	["Easter Bow"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_bow_easter-sharedassets0.assets-115.png"
	},
	["Safari Shirt"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_shirt_safari1-resources.assets-3434.png"
	},
	["Police Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_police.png",
		description = "<br>''[[Coupon Codes|Northernlion Live Super Show]]''"
	},
	["Fly Swatter"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_flyswatter-resources.assets-4880.png"
	},
	["DreamHack Umbrella"] = {
		class = "Umbrella",
		rarity = 5,
		file = "Umbrella_base_dreamhack-resources.assets-1925.png",
		description = "<br>''[[Coupon Codes|DreamHack Dallas 2019]]''"
	},
	["Baseball Cap (Rainbow)"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_baseball_rainbow-resources.assets-4813.png",
		description = "<br>''[[Coupon Codes|Pride 2019]]''"
	},
	["Rainbow Umbrella"] = {
		class = "Umbrella",
		rarity = 5,
		file = "Umbrella_base_rainbow-resources.assets-2272.png",
		description = "<br>''[[Coupon Codes|Pride 2019]]''"
	},
	["SAW Security Uniform"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_SAWguard.png"
	},
	["Stars & Stripes Baseball Bat"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_baseballbat_USA-resources.assets-3928.png",
		description = "<br>''[[Coupon Codes|Independence Day 2019]]''"
	},
	["Hockey Stick"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_hockeystick-resources.assets-5024.png",
		description = "<br>''[[Coupon Codes|Canada Day 2019]]''"
	},
	["Mountie Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_mountie-resources.assets-4207.png",
		description = "<br>''[[Coupon Codes|Canada Day 2019]]''"
	},
	["Stars & Stripes Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_unclesam-resources.assets-4020.png",
		description = "<br>''[[Coupon Codes|Independence Day 2019]]''"
	},
	["Mountie Outfit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_mountie-resources.assets-3210.png",
		description = "<br>''[[Coupon Codes|Canada Day 2019]]''"
	},
	["Uncle Sam Outfit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_suit_unclesam-resources.assets-3571.png",
		description = "<br>''[[Coupon Codes|Independence Day 2019]]''"
	},
	["Praise Banan"] = {
		class = "Emote",
		rarity = 5,
		file = "Praise_banan_emote.png"
	},
	["No Dancing Sign"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_sign.png"
	},
	["Yellow Lei Hula Skirt"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_bikini_hula.png"
	},
	Lifebuoy = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_lifebuoy.png"
	},
	["Fish Swim Shorts"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_swimshorts_fish.png"
	},
	["Straw Umbrella"] = {
		class = "Umbrella",
		rarity = 5,
		file = "Umbrella_straw.png"
	},
	["Duck Floatie"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_floatie_duck.png"
	},
	["Skunk Bomb Tee"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tshirt_skunk.png"
	},
	["Skunk Nose Clip"] = {
		class = "Beard",
		rarity = 5,
		file = "Moustache_noseclip.png"
	},
	["Skunk Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_skunk.png"
	},
	Popsicle = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_popsicle.png"
	},
	["Fruit Skewer"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_skewer_fruit.png"
	},
	["Fruit Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_fruit_hat.png"
	},
	["Squid Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_squid-resources.assets-233.png",
		description = "<br>''[[Coupon Codes|Super Squids]]''"
	},
	["Egg Umbrella"] = {
		class = "Umbrella",
		rarity = 5,
		file = "Umbrella_egg.png",
		description = "<br>''[[Coupon Codes|Northernlion Live Super Show]]''"
	},
	["Josh Umbrella"] = {
		class = "Umbrella",
		rarity = 5,
		file = "Umbrella_base_josh.png",
		description = "<br>''[[Coupon Codes|Northernlion Live Super Show]]''"
	},
	["Skull Beanie"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_beanie_skull.png",
		description = "<br>''[[Coupon Codes|Northernlion Live Super Show]]''"
	},
	["Jeans Vest"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_jeanvest.png",
		description = "<br>''[[Coupon Codes|Northernlion Live Super Show]]''"
	},
	["Police Outfit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_police.png",
		description = "<br>''[[Coupon Codes|Northernlion Live Super Show]]''"
	},
	["Velvet Robe"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_robe_velvet.png",
		description = "<br>''[[Coupon Codes|Northernlion Live Super Show]]''"
	},
	["Red Button Up Shirt"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_shirt_fullred.png",
		description = "<br>''[[Coupon Codes|Northernlion Live Super Show]]''"
	},
	["Red Striped Shirt"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_shirt_redstriped.png",
		description = "<br>''[[Coupon Codes|Northernlion Live Super Show]]''"
	},
	["Chicken Dinner Gravestone"] = {
		class = "Gravestone",
		rarity = 5,
		file = "Gravestone_chicken-resources.assets-320.png"
	},
	["Cheese Goudara's Beret"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_military_rebellion-resources.assets-2686.png"
	},
	["Prisoner Outfit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_prisoner-resources.assets-5055.png"
	},
	["Gem Reveal"] = {
		class = "Emote",
		rarity = 5,
		file = "Gem_Reveal_emote.png"
	},
	["Banan Pew Pew"] = {
		class = "Emote",
		rarity = 5,
		file = "Banan_Pew_Pew_emote.png"
	},
	["Blow a Kiss"] = {
		class = "Emote",
		rarity = 5,
		file = "Blow_a_Kiss_emote.png"
	},
	["Witch's Broom"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_broom_witch-resources.assets-1678.png"
	},
	Machete = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_machete-resources.assets-678.png"
	},
	["Spider Dress"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_dress_spider-resources.assets-620.png"
	},
	["Witch Costume"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_witch_outfit-resources.assets-845.png"
	},
	["Mariachi Outfit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_mariachi_outfit-resources.assets-654.png"
	},
	["Skeleton Costume"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_skeleton_costume-resources.assets-1068.png"
	},
	["Halloween Tee"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tshirt_halloween-resources.assets-1744.png"
	},
	["Vampire Costume"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_vampire_outfit-resources.assets-233.png"
	},
	["Mariachi Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_mariachi-resources.assets-1281.png"
	},
	["Witch Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_witch-resources.assets-1182.png"
	},
	["Killer Hockey Mask"] = {
		class = "Glasses",
		rarity = 5,
		file = "Glasses_hockeymask-resources.assets-573.png"
	},
	["Black Masquerade Mask"] = {
		class = "Glasses",
		rarity = 5,
		file = "Glasses_masquerade_mask_black-resources.assets-1420.png"
	},
	["Pumpkin Umbrella"] = {
		class = "Umbrella",
		rarity = 5,
		file = "Umbrella_pumpkin-resources.assets-458.png"
	},
	["Candy Corn Umbrella"] = {
		class = "Umbrella",
		rarity = 5,
		file = "Umbrella_candy_corn-resources.assets-1681.png"
	},
	["Spider Web Umbrella"] = {
		class = "Umbrella",
		rarity = 5,
		file = "Umbrella_spider_web-resources.assets-903.png"
	},
	["Super Bat Costume"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_superbat-resources.assets-1283.png"
	},
	["Pumpkin Gravestone"] = {
		class = "Gravestone",
		rarity = 5,
		file = "Gravestone_pumpkin-resources.assets-1615.png"
	},
	["Super Bat Mask"] = {
		class = "Glasses",
		rarity = 5,
		file = "Glasses_super_bat_mask-resources.assets-864.png"
	},
	["Scarecrow Outfit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_scarecrow-resources.assets-222.png"
	},
	["Straw Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_straw-resources.assets-364.png"
	},
	["Pumpkin Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_pumpkin-resources.assets-362.png"
	},
	["Caveman Costume"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_caveman-resources.assets-994.png"
	},
	["Steam Punk Monocle"] = {
		class = "Glasses",
		rarity = 5
	},
	["Gallery Target Tee"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tshirt_target-resources.assets-698.png"
	},
	["5 Point Tee"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tshirt_5points-resources.assets-1204.png"
	},
	["Stretching Routine"] = {
		class = "Emote",
		rarity = 5,
		file = "Stretch_Emote_Icon.png"
	},
	["Chef Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_chef-resources.assets-1194.png"
	},
	Keikogi = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_keikogi-resources.assets-868.png"
	},
	["POW! Mallet"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_mallet-resources.assets-977.png"
	},
	["Peppermint Thomas Gun"] = {
		class = "GunSkin",
		rarity = 5,
		file = "Gun-thomas_gun_peppermint.png",
		description = "<br>''[[Super CRISPRmas]]''",
		link = "Thomas Gun"
	},
	["Santa Beard"] = {
		class = "Beard",
		rarity = 5,
		file = "Beard3_white-resources.assets-1071.png"
	},
	["Red Nose"] = {
		class = "Beard",
		rarity = 5,
		file = "Moustache_rednose-resources.assets-833.png"
	},
	["Spearmint Candy Cane"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_candycane_green-resources.assets-753.png"
	},
	["Festive Candy Cane"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_candycane_mix-resources.assets-1361.png"
	},
	["Christmas Tree"] = {
		class = "MeleeSkin",
		rarity = 5,
		name = "CRISPRmas Tree",
		file = "Melee_christmas_tree-resources.assets-414.png"
	},
	["Slay Bells"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_sleighbells-resources.assets-1227.png"
	},
	["Elf Outfit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_christmas_elf-resources.assets-1267.png"
	},
	["Festive Lights Outfit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_christmas_lights-resources.assets-1086.png"
	},
	["New Years Dress"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_dress_new_years.png"
	},
	["CRISPRmas Sweater"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_sweater_CRISPRmas-resources.assets-452.png"
	},
	["Wreath Necklace"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_wreath-resources.assets-793.png"
	},
	["Anniversary Cake Gravestone"] = {
		class = "Gravestone",
		rarity = 5,
		file = "Gravestone-cake.png",
		description = "<br>''[[Coupon Codes|Super 1 Year Anniversary]]''"
	},
	["Antlers Headband"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_antlers-resources.assets-1283.png"
	},
	["Elf Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_christmas_elf-resources.assets-1259.png"
	},
	["New Years Party Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_new_years.png"
	},
	["Pixile Party Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_party_pixile.png",
		description = "<br>''[[Coupon Codes|Super 1 Year Anniversary]]''"
	},
	["Festive Umbrella"] = {
		class = "Umbrella",
		rarity = 5,
		file = "Umbrella_christmas-resources.assets-623.png"
	},
	["Present Umbrella"] = {
		class = "Umbrella",
		rarity = 5,
		file = "Umbrella_present-resources.assets-665.png"
	},
	["Snowflake Umbrella"] = {
		class = "Umbrella",
		rarity = 5,
		file = "Umbrella_snowflakes-resources.assets-802.png"
	},
	["Wooden Chopsticks"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_chopsticks.png",
		description = "<br>''[[Year of the Super Rat]] rewards''"
	},
	Dadao = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_dadao.png",
		description = "<br>''[[Year of the Super Rat]] rewards''"
	},
	["Red Paper Fan"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_fan.png",
		description = "<br>''[[Year of the Super Rat]] rewards''"
	},
	Guandao = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_guandao.png",
		description = "<br>''[[Year of the Super Rat]] rewards''"
	},
	["Mandarin Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_mandarin.png",
		description = "<br>''[[Year of the Super Rat]] rewards''"
	},
	["Bamboo Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_ricepaddy.png",
		description = "<br>''[[Year of the Super Rat]] rewards''"
	},
	["Pink Lunar Shirt"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_lunar_shirt_pink.png",
		description = "<br>''[[Year of the Super Rat]] rewards''"
	},
	Hanfu = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_hanfu.png",
		description = "<br>''[[Year of the Super Rat]] rewards''"
	},
	["Yellow Tang Suit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_tang_suit_yellow.png",
		description = "<br>''[[Year of the Super Rat]] rewards''"
	},
	["Blue Robe"] = {
		class = "Clothes",
		rarity = 5
	},
	["Lucky Hunting Rifle"] = {
		class = "GunSkin",
		rarity = 5,
		file = "Gun_rifle_stpatrick-resources.assets-190.png",
		description = "<br>''[[St. Pawtrick's Day 2020]]''",
		link = "Hunting Rifle"
	},
	["Rancher Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_rancher-resources.assets-847.png"
	},
	["Rancher Outfit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_rancher-resources.assets-709.png"
	},
	["Lucky Dress"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_dress_lucky-resources.assets-1380.png"
	},
	["Lucky Shirt"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_shirt_lucky-resources.assets-704.png"
	},
	["Lucky Bow"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_lucky_bow-resources.assets-1043.png"
	},
	["Skydiving Camera Helmet"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_skydive_camera.png"
	},
	["Blue Skydiving Jumpsuit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_skydiving_blue.png"
	},
	["Red Skydiving Jumpsuit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_skydiving_red.png"
	},
	["Banana Umbrella"] = {
		class = "Umbrella",
		rarity = 5,
		file = "Umbrella_banana-resources.assets-511.png"
	},
	Raincoat = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_raincoat.png"
	},
	["Sakura Kimono"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_kimono_sakura.png",
		description = "<br>''[[Coupon Codes|Cherry Blossom Season]]''"
	},
	["Sakura Fan"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_sakura_fan.png",
		description = "<br>''[[Coupon Codes|Cherry Blossom Season]]''"
	},
	["Sakura Umbrella"] = {
		class = "Umbrella",
		rarity = 5,
		file = "Umbrella_sakura.png",
		description = "<br>''[[Coupon Codes|Cherry Blossom Season]]''"
	},
	["Easter Suspenders"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_suspenders_easter-resources.assets-428.png"
	},
	["Easter Suit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_suit_easter-resources.assets-1508.png"
	},
	["Easter Dress"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_dress_easter-resources.assets-1371.png"
	},
	["Red Plaid Dress"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_dress_plaid_red-resources.assets-1193.png"
	},
	["Chocolate Bar"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_chocolatebar-resources.assets-563.png"
	},
	["Flower Crown"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_flower_crown-resources.assets-398.png"
	},
	["Easter Sun Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_easter_sun-resources.assets-536.png"
	},
	["Eggshell Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_eggshell-resources.assets-580.png"
	},
	["Flower Glasses"] = {
		class = "Glasses",
		rarity = 5,
		file = "Glasses_flowers-resources.assets-752.png"
	},
	["Sarcophagus Gravestone"] = {
		class = "Gravestone",
		rarity = 5
	},
	["Skullcat Scepter"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_scepter_skullcat.png"
	},
	["Badminton Racket"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_badminton.png"
	},
	["Yellow Plastic Shovel"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_shovel_plastic_yellow.png"
	},
	["Vintage Round Sunglasses"] = {
		class = "Glasses",
		rarity = 5,
		file = "Glasses_vintage_round.png"
	},
	["Blue Sport Sunglasses"] = {
		class = "Glasses",
		rarity = 5,
		file = "Glasses_sport_blue.png"
	},
	["Vintage Floral Dress"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_dress_vintage_floral.png"
	},
	["Orange Pastel Overalls"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_overall_pastel_orange.png"
	},
	["White Bermuda Shorts"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_pants_bermuda_white.png"
	},
	["Blue Polo Shirt"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_shirt_polo_blue.png"
	},
	["Vintage Floral Swimsuit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_swimsuit_flowers.png"
	},
	["Canvas Gravestone"] = {
		class = "Gravestone",
		rarity = 5,
		file = "Gravestone-canvas.png"
	},
	["Artist Smock"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_artist.png"
	},
	["Artist Beret"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_artist.png"
	},
	["Golden Paintbrush"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_paintbrush_golden.png"
	},
	["Digital Stylus"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_digital_stylus.png"
	},
	["Target Gravestone"] = {
		class = "Gravestone",
		rarity = 5,
		file = "Gravestone-target.png"
	},
	["Brown Archer Outfit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_archer_brown.png"
	},
	["Green Archer Outfit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_archer_green.png"
	},
	["Archer Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat_archer.png"
	},
	["Yellow Crayon"] = {
		class = "MeleeSkin",
		rarity = 5
	},
	["Bat Wings"] = {
		class = "Clothes",
		rarity = 5,
		name = "Bat Wing Outfit",
		file = "Clothes_bat_wings.png"
	},
	["Frankenstein Outfit"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_frankenstein.png"
	},
	["Pumpkin Costume"] = {
		class = "Clothes",
		rarity = 5,
		file = "Clothes_pumpkin_costume.png"
	},
	["Howl Mask"] = {
		class = "Glasses",
		rarity = 5
	},
	["Cauldron Gravestone"] = {
		class = "Gravestone",
		rarity = 5,
		file = "Cauldron-gravestone.png"
	},
	["Skeleton AK"] = {
		class = "GunSkin",
		rarity = 5,
		file = "Gun-ak_skeleton.png",
		description = "<br>''[[Super Halloween 2020]]''",
		link = "AK"
	},
	["Candy Corn Sniper"] = {
		class = "GunSkin",
		rarity = 5,
		file = "Gun-sniper_candycorn.png",
		description = "<br>''[[Super Halloween 2020]]''",
		link = "Sniper"
	},
	["Devil Pitchfork"] = {
		class = "MeleeSkin",
		rarity = 5,
		file = "Melee_devil_pitchfork.png"
	},
	["Bolts Hat"] = {
		class = "Hat",
		rarity = 5,
		file = "Hat bolts.png"
	},
	["Skull Bow"] = {
		class = "Hat",
		rarity = 5
	},
	["Festive Bow"] = {
		class = "Clothes",
		rarity = 5
	},
	["Green Festive Scarf"] = {
		class = "Clothes",
		rarity = 5
	},
	["Yellow Knitted Sweater"] = {
		class = "Clothes",
		rarity = 5
	},
	["Light Blue Winter Dress"] = {
		class = "Clothes",
		rarity = 5
	},
	["Santa Dress"] = {
		class = "Clothes",
		rarity = 5
	},
	["Ski Pole"] = {
		class = "MeleeSkin",
		rarity = 5
	},
	Icicle = {
		class = "MeleeSkin",
		rarity = 5
	},
	["Red Crayon"] = {
		class = "MeleeSkin",
		rarity = 5
	},
	["Foggy Glasses"] = {
		class = "Glasses",
		rarity = 5
	},
	["2nd Anniversary Cake Gravestone"] = {
		class = "Gravestone",
		rarity = 5
	},
	["Pixile Umbrella"] = {
		class = "Umbrella",
		rarity = 5
	},
	["SAR Tickets x3"] = {
		file = "Currency Tickets x3.png",
		class = "Currency",
		rarity = 5,
		link = "Currency"
	}
}