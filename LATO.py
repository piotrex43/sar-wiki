# LATO wiki bot is a project for executing bot actions on MediaWiki wikis.
# Copyright (C) 2019 Frisk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import requests, json, urllib, time, re

class MWError(Exception):
    pass

class PageDoesNotExist(Exception):
    pass

with open("auth.json") as sfile:
	settings = json.load(sfile)

class frisk_wiki_bot(object):
	s = requests.Session()
	s.headers.update({"user-agent": "FriskWikiBot/1.2"})
	edit_token = {}
	logged_in = []
	def handle_mw_errors(self, request):
		if "errors" in request:
			print(request["errors"])
			if request["errors"]["code"] == "ratelimited":
				print("REQUEST RATELIMITED ^")
				return request
			else:
				raise MWError
		return request
	def log_in(self, wiki):
		url = "https://{}.fandom.com/api.php".format(wiki)
		try:
			response = self.handle_mw_errors(self.s.post(url, data={'action': 'query', 'format': 'json', 'utf8': '', 'meta': 'tokens', 'type': 'login'}))
			response = self.handle_mw_errors(self.s.post(url, data={'action': 'login', 'format': 'json', 'utf8': '', 'lgname': settings["login"], 'lgpassword': settings["password"], 'lgtoken': response.json()['query']['tokens']['logintoken']}))
		except ValueError:
			print("Błąd połączenia")
			return -1
		except MWError:
			print("Unable to log in")
			return -1
		try:
			if response.json()['login']['result']=="Success":
				print("Logged in successfully to {}".format(url))
				self.logged_in.append(wiki)
				return True
			else:
				 print("Logging in not successful {reason}".format(reason=response.json()))
		except KeyError:
			print("Logging in not successful {reason}".format(reason=response.json()))
	def get_token(self, wiki, token_type="csrf"):
		if wiki not in self.logged_in:
			self.log_in(wiki)
		url = "https://{}.fandom.com/api.php".format(wiki)
		try:
			request = self.handle_mw_errors(self.s.get(url + '?action=query&format=json&assert=user&meta=tokens&type={}'.format(token_type)).json())
		except MWError:
			return False
		try:
			edit_token = request['query']['tokens']['{}token'.format(token_type)]
		except KeyError:
			code =  request.json()['error']['code']
			if(code=="assertuserfailed"):
				print("Seems like we are not logged in?")
			return False
		if(edit_token=="+\\"):
			print("Zwrócono pusty token (jesteś niezalogowany?)")
		else:
			print("Successfully retrived the token")
			self.edit_token[wiki] = edit_token
			return True
		return False
	def create_article(self, wiki, title, content, summary):
		url = "https://{}.fandom.com/api.php".format(wiki)
		if wiki not in self.edit_token:
			if not self.get_token(wiki):
				return "Error!"
		try:
			request = self.handle_mw_errors(self.s.post(url, data={'action': 'edit', 'assert': 'user', 'format': 'json', 'utf8': '','summary': summary, 'title': title, 'bot': "", 'createonly': "", 'token': self.edit_token[wiki], 'text': content}))
		except MWError:
			return
		self.edit_check(request, title)
	def fetch_art(self, wiki, article):
		url = "https://{}.fandom.com/api.php".format(wiki)
		try:
			article_content = self.handle_mw_errors(self.s.get("{endpoint}?action=query&format=json&prop=revisions&titles={article}&rvprop=content".format(endpoint=url, article=urllib.parse.quote_plus(article))).json())
		except MWError:
			raise
		article_content = article_content["query"]["pages"]
		if "missing" in list(article_content.values())[0]:
			raise PageDoesNotExist
		#if list(article_content.values())[0]['ns'] not in [0, 6]:
		#	return "Page not in allowed namespace"
		try:
			content = list(article_content.values())[0]['revisions'][0]['*']
		except KeyError:
			print( "KeyError while retrieving page data")
			return None
		return content
	def edit_check(self, request, title):
		try:
			if(request.json()['edit']['result']=="Success"):
				print("Successfully made the edit to the page " + title)
				return True
			elif(request.json()['edit']['result']=="Failure"):
				print("Edit did not succeed, the error code is: " + request.json()['edit']['code'])
			else:
				print("Edit did not succeed, the reason is: " + request.text)
		except KeyError:
			edit_token = request.json()['error']['code']
			print("Edit did not succeed, the reason is: " + request.text)
			if(edit_token=="assertuserfailed"):
				print("Seems like we are not logged in?")
		return False
	def replace_content(self, wiki, content, title, summary):
		url = "https://{}.fandom.com/api.php".format(wiki)
		if wiki not in self.edit_token:
			if not self.get_token(wiki):
				return "Error!"
		try:
			request = self.handle_mw_errors(self.s.post(url, data={'action': 'edit', 'assert': 'user', 'format': 'json', 'utf8': '','summary': summary, 'title': title, 'bot': "", 'nocreate': "", 'token': self.edit_token[wiki], 'text': content}))
		except MWError:
			return
		return self.edit_check(request, title)
	def append_content(self, wiki, content, title, summary):
		url = "https://{}.fandom.com/api.php".format(wiki)
		if wiki not in self.edit_token:
			if not self.get_token(wiki):
				return "Error!"
		try:
			request = self.handle_mw_errors(self.s.post(url, data={'action': 'edit', 'assert': 'user', 'format': 'json', 'utf8': '','summary': summary, 'title': title, 'bot': "", 'nocreate': "", 'token': self.edit_token[wiki], 'appendtext': content}))
		except MWError:
			return
		self.edit_check(request, title)
	def remove_article(self, wiki, title, summary):
		url = "https://{}.fandom.com/api.php".format(wiki)
		if wiki not in self.edit_token:
			if not self.get_token(wiki):
				return "Error!"
		request = self.s.post(url, data={'action': 'delete', 'assert': 'user', 'format': 'json', 'utf8': '', 'reason': summary, 'title': title, 'token': self.edit_token[wiki]})
		try:
			if(request.json()['delete']['reason']):
				print("Successfully removed article " + title)
				return True
			elif(request.json()['delete']['reason']):
				print("Deletion did not succeed, the error code is: " + request.json()['delete']['code'])
			else:
				print("Deletion did not succeed, the reason is: " + request.text)
		except KeyError:
			edit_token = request.json()['error']['code']
			print("Deletion did not succeed, the reason is: " + request.text)
			if(edit_token=="assertuserfailed"):
				print("Seems like we are not logged in?")
		return False
	def null_edit(self, wiki, title):
		processed = self.fetch_art(wiki, title)
		print (processed)
		if self.append_content(wiki, "", title, "Null edit (if you see this edit it means something gone horribly wrong)"):
			return ":thumbsup:"
	def move(self, wiki, oldtitle, newtitle, reason, redirect=False):
		url = "https://{}.fandom.com/api.php".format(wiki)
		if wiki not in self.edit_token:
			if not self.get_token(wiki):
				return "Error!"
		data = {'action': 'move', 'assert': 'user', 'format': 'json', 'utf8': '','reason': reason, 'from': oldtitle, 'token': self.edit_token[wiki], 'to': newtitle}
		if redirect is False:
			data["noredirect"] = ""
		try:
			request = self.handle_mw_errors(self.s.post(url, data=data).json())
		except MWError:
			time.sleep(30)
			return
		print (request.json())
		print ("Good move from {} to {}".format(oldtitle, newtitle))
	def get_category_members(self, wiki, category):
		url = "https://{}.fandom.com/api.php".format(wiki)
		try:
			category = self.handle_mw_errors(self.s.get(
				"{url}?action=query&format=json&&list=categorymembers&cmtitle=Category%3A{cat}&cmprop=title&cmlimit=500".format(
					url=url, cat=urllib.parse.quote_plus(category))).json())
		except MWError:
			raise
		new_list = []
		for item in category['query']['categorymembers']:
			new_list.append(item["title"])
		return new_list
	def get_all_pages(self, wiki, namespace=0):
		url = "https://{}.fandom.com/api.php".format(wiki)
		try:
			result = self.handle_mw_errors(self.s.get("{url}?action=query&format=json&list=allpages&apnamespace={ns}&aplimit=max".format(url=url, ns=namespace)).json())
		except MWError:
			return None
		article_list = []
		for article in result["query"]["allpages"]:
			article_list.append(article["title"])
		return article_list
	def fix_links(self, content):
		regex = r"\[\[(?P<first>((\w+) ?)*)\|(?P<second>((\w+) ?)*)\]\]"
		found = re.finditer(regex, content)
		for match in found:
			groups = match.groups()
			new_string = ""
			addition = ""
			incorrect = True
			for num, letter in enumerate(groups[3]):
				if len(groups[0]) > len(groups[3]):
					incorrect = False
					break
				if len(groups[0]) >= num + 1:
					if num != 0:
						if groups[0][num] == letter:
							new_string += groups[0][num]
						else:
							incorrect = False
							break
					elif groups[0][num].lower() == letter.lower() and num == 0:
						new_string += groups[3][0]
					else:
						incorrect = False
						break
				else:
					addition += letter
			if incorrect and ' ' not in addition:
				print("Incorrect!")
				content = content.replace("[[" + groups[0] + "|" + groups[3] + "]]",
				                          "[[" + new_string + "]]" + addition)
		return content
	def getUserId(self, user, wiki="help"):
		try:
			userId = self.handle_mw_errors(self.s.get("https://{}.fandom.com/api.php?action=query&format=json&list=users&usprop=centralids&ususers={}".format(wiki, urllib.parse.quote_plus(user))).json())
			if userId is not None:
				return userId["query"]["users"][0]["userid"]
		except:
			raise
	def editField(self, wiki, userid, field, content):
		if wiki not in self.edit_token:
			if not self.get_token(wiki):
				return "Error!"
		try:
			data = {'action': 'profile', 'assert': 'user', 'format': 'json', 'utf8': '', 'do': 'editField', 'user_id': userid, 'field': field, 'text': content, 'token':  self.edit_token[wiki]}
			edit = self.s.post("https://{}.fandom.com/api.php".format(wiki), data = data)
			return edit
		except:
			raise

actions = frisk_wiki_bot()


